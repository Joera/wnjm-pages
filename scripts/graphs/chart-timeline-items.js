let ChartTimeLineItems = function ChartTimeLineItems(config,svg) {

    let draw = function draw(data,colours) {


        svg.pastTime = svg.layers.data
            .append('rect')
            .attr('class','time-passed');

        svg.bar = svg.layers.data.selectAll(".bar")
            .data(data);
        
        svg.bar.exit().remove();

        svg.barEnter = svg.bar
            .enter()
            .append("rect")
            .attr("class", function(d,i) {
                return "bar"; // + sluggify(d.status) + "
            });

        svg.barLabels = svg.layers.data.selectAll(".barLabel")
            .data(data);

        svg.barLabels.exit().remove();

        svg.barLabelsEnter = svg.barLabels
            .enter()
            .append('text')
            .attr('class','barLabel small-label')
            .attr('x', 0)
            .attr('dx', '0px')
            .attr('dy', (d) => {
                return  d.offset + 'px';
            })
            .style("text-anchor", "start")

            ;

        svg.barDates = svg.layers.data.selectAll(".barDates")
            .data(data);

        svg.barDates.exit().remove();

        svg.barDatesEnter = svg.barDates
            .enter()
            .append('text')
            .attr('class','tickSize')
            .attr('x', 0)
            .attr('dx', '0px')
            .attr('dy', '10px')
            .style("text-anchor", "end")

        ;



    }

    let redraw = function redraw(dimensions,xScale,yScale) {

        svg.pastTime
            .style("fill", "white")
            .attr("x", function(d) {
                return config.margin.left + 1;
            })
            .attr("y", -2)
            .attr("height", 0)
            .attr("width", 11)
            .transition()
            .delay(1000)
            .duration(1000)
            .attr("height", d => yScale.timeline(moment()));

        svg.bar
            .merge(svg.barEnter)
            .attr("x", function(d) {
                return config.margin.left + 1;
            })
            .attr("y", function(d) { return config.margin.top + yScale.timeline(moment(d['_startDate'])); })
            .attr("dy",-6)
            .attr("height", function(d) {

                return (moment(d['_endDate']) > moment(d['_startDate'])) ?  yScale.timeline(moment(d['_endDate'])) - yScale.timeline(moment(d['_startDate']))  : 12;
            })
            .style("fill", function(d) {

                return (moment(d['_endDate']) > moment()) ? purple : middelgrijs;
            })
            .transition()
            .duration(500)
            .attr("width", function(d) {
                return 11;
            });

        ;

        svg.barLabels
            .merge(svg.barLabelsEnter)
            .text(function(d) {
                    return d.label;
            })
            .attr('transform', function(d) {

                if (moment(d['_endDate']) > moment(d['_startDate'])) {
                    let half = (yScale.timeline(moment(d['_endDate'])) - yScale.timeline(moment(d['_startDate']))) / 2;
                    let pos = -6 + half + config.margin.top + yScale.timeline(moment(d['_startDate']));
                    return 'translate(' + 380 + ',' + pos + ')';
                } else {
                    return 'translate(' + 380 + ',' + config.margin.top + yScale.timeline(moment(d['_startDate'])) + ')';
                }
            })
            .attr('fill-opacity', 0)
            .transition()
            .delay(500)
            .duration(500)
            .attr('transform', function(d) {

                if (moment(d['_endDate']) > moment(d['_startDate'])) {
                    let half = (yScale.timeline(moment(d['_endDate'])) - yScale.timeline(moment(d['_startDate']))) / 2;
                    let pos = -6 + half + config.margin.top + yScale.timeline(moment(d['_startDate']));
                    return 'translate(' + (config.margin.left + 20) + ',' + pos + ')';
                } else {
                    return 'translate(' + (config.margin.left + 20) + ',' + config.margin.top + yScale.timeline(moment(d['_startDate'])) + ')';
                }
            })
            .attr('fill-opacity', 1);



        svg.barDates
            .merge(svg.barDatesEnter)
            .text(function(d) {

                if (moment(d['_endDate']) > moment(d['_startDate'])) {

                    return moment(d._startDate).format('D/M') + ' t/m ' + moment(d._endDate).format('D/M');

                } else {
                    return moment(d._startDate).format('D/M');
                }
            })
            .attr('transform', function(d) {

                if (moment(d['_endDate']) > moment(d['_startDate'])) {
                    let half = (yScale.timeline(moment(d['_endDate'])) - yScale.timeline(moment(d['_startDate']))) / 2;
                    let pos = -6 + half + config.margin.top + yScale.timeline(moment(d['_startDate']));
                    return 'translate(' + 380 + ',' + pos + ')';
                } else {
                    return 'translate(' + 380 + ',' + config.margin.top + yScale.timeline(moment(d['_startDate'])) + ')';
                }
            })
            .attr('fill-opacity', 0)
            .transition()
            .delay(500)
            .duration(500)
            .attr('transform', function(d) {

                if (moment(d['_endDate']) > moment(d['_startDate'])) {
                    let half = (yScale.timeline(moment(d['_endDate'])) - yScale.timeline(moment(d['_startDate']))) / 2;
                    let pos = -6 + half + config.margin.top + yScale.timeline(moment(d['_startDate']));
                    return 'translate(' + (config.margin.left - 5) + ',' + pos + ')';
                } else {
                    return 'translate(' + (config.margin.left - 5) + ',' + config.margin.top + yScale.timeline(moment(d['_startDate'])) + ')';
                }
            })
            .attr('fill-opacity', 1);


    }


    return  {
        draw : draw,
        redraw : redraw
    }
}


