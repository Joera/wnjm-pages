let ChartYScale = function ChartYScale(config,dimensions,scale) {

    let set = function set(data,property,property_two) {

        let endDate = new Date();
        let minValue;

        // kun je dit meegeven als conditional

        if(config.minValue) {
            minValue = (d3.max(data, d => d[property]) > 20000) ? config.minValue : 900;
        } else {
            minValue = 0; //
        }

        scale.linear = d3.scaleLinear()
            .domain([
                minValue,
                config.maxValue || d3.max(data, d => d[property])
            ]).nice();

        let arrayOfCumulatedValues = [];

        for (let p = 0; p < data.length; p++) {
            for (let i = 0; i < data[p].length - 1; i++) {
                for (let e = 0; e < data.length; e++) {
                    arrayOfCumulatedValues.push(data[e][i][1]); // v
                }
            }
        }

        scale.stacked = d3.scaleLinear()
            .domain([
                minValue,
                config.maxValue || d3.max(arrayOfCumulatedValues)
            ])
            .nice();

        scale.timeline = d3.scaleTime()
            .domain([
                d3.min(data, d => moment(d[property]).subtract('days', 3)),  //
                d3.max(data, d => moment(d[property_two]).add('days', 3))
            ]);

        return scale;
    }


    let reset = function reset(dimensions,newScale) {

        newScale.linear
            .range([dimensions.height, 0]);

        newScale.stacked
            .range([(config.fixedHeight || dimensions.height), 0]);

        newScale.timeline
            .range([0,dimensions.height]);

        return newScale;
    }


    return {
        set : set,
        reset : reset,
    }
}


