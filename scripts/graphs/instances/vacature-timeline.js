var vacature_timeline = function(element) {

    let chartObjects = ChartObjects();
    let config = chartObjects.config();
    let dimensions = chartObjects.dimensions();
    let svg = chartObjects.svg();
    let xScale = chartObjects.xScale();
    let yScale = chartObjects.yScale();
    let axes = chartObjects.axes();
    let functions = chartObjects.functions();

    config.margin.top = 0;
    config.margin.bottom = 30;
    config.margin.left = 120;
    config.margin.right = 0;
    config.padding.top = 0;
    config.padding.bottom = 30;
    config.padding.left = 0;
    config.padding.right = 0;
    // name of first column with values of bands on x axis


//    config.yParameter = 'behandeling';  // is being set in type function
    // config.fixedHeight = 160;
    // config.minValue = 0;
    // config.maxValue = 60000;


  //  config.yParameter = '_startDate';
    //
    let colours = [];

    // get dimensions from parent element
    let chartDimensions = ChartDimensions(element,config);
    dimensions = chartDimensions.get(dimensions);

    // create svg elements without data
    let chartSVG = ChartSVG(element,config,dimensions,svg);
    let chartYScale = ChartYScale(config,dimensions,yScale);
    let chartAxis = ChartAxis(config,svg);
    let chartTimeLineItems =  ChartTimeLineItems(config,svg);

    chartAxis.drawYAxis();


        let data = [];
        let dataList = [].slice.call(document.querySelectorAll('ul.timeline-data li'));

        dataList.forEach( (li,i) => {

            console.log('hi');

            if (li.getAttribute('data-start-date')) {

                let offset = 10;

                if(i > 0) {
                    let start = moment(li.getAttribute('data-start-date'));
                    let end = moment(li.getAttribute('data-end-date'));
                    let endPrevious  = moment(dataList[i - 1].getAttribute('data-end-date'));
                    let startNext  = moment(dataList[i - 1].getAttribute('data-start-date'));

                    if (startNext.diff(end,'days') < 2) {
                        offset = 9;
                    }

                      if (start.diff(endPrevious,'days') < 2) {
                          offset = 11;
                      }
                }


                data.push({
                    _startDate: li.getAttribute('data-start-date'),
                    _endDate: li.getAttribute('data-end-date'),
                    label: li.getAttribute('data-label'),
                    offset: offset
                });
            }
        });


        console.log(data);

        function draw(data) {

            yScale = chartYScale.set(data,'_startDate','_endDate');
            chartTimeLineItems.draw(data,colours);
        }

        function redraw() {
            // on redraw chart gets new dimensions
            dimensions = chartDimensions.get(dimensions);
            chartSVG.redraw(dimensions);
            // new dimensions mean new scales
            yScale = chartYScale.reset(dimensions,yScale);

            console.log(dimensions);
            // new scales mean new axis
            chartAxis.redrawYTimelineAxis(yScale,axes);
            // redraw data
            chartTimeLineItems.redraw(dimensions,xScale,yScale);

        }

     //   chartStackedArea.draw(stackedData,colours);
        // further drawing happens in function that can be repeated.
        draw(data);
        redraw();
        // for example on window resize
        window.addEventListener("resize", redraw, false);

    // });
}