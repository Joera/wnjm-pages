let ChartAxis = function ChartAxis(config,svg) {

    let drawXAxis = function drawXAxis() {

        svg.xAxis = svg.layers.axes.append("g")
            .attr('class', 'x-axis');
    }

    let redrawXBandAxis = function redrawXAxis(dimensions,xScale,axes,alternateTicks) {

        axes.xBand = d3.axisBottom(xScale.band);

        svg.xAxis
            .attr("transform", "translate(" + config.margin.left + "," + (dimensions.height + config.padding.top) + ")")  //
            .call(axes.xBand);

        if (alternateTicks) {

            let alternate_text = false;

            d3.selectAll("g.x-axis g.tick text")
                .attr("y", function () {
                    if (alternate_text) {
                        alternate_text = false;
                        return 26;
                    } else {
                        alternate_text = true;
                        return 10;
                    }
                });
        }
    }

    let redrawXTimeAxis = function redrawXAxis(dimensions,scales,axes,ticks) {

        axes.xTime = d3.axisBottom(scales.time);

        if(ticks) {
            axes.xTime
                .ticks(d3.timeMonth.every(1))
                .tickFormat(function(date){
                    return (d3.timeYear(date) < date) ? localTime.format('%b')(date) : localTime.format('%Y')(date);
                });
        } else {
            axes.xTime
                .tickValues([]);
        }

        svg.xAxis
            .attr("transform", "translate(" + config.padding.left + "," + (dimensions.height + config.padding.top) + ")")  //
            .call(axes.xTime);
    }

    let drawYAxis = function drawYAxis() {

        svg.yAxis = svg.layers.axes.append("g")
            .attr('class', 'y-axis')
            .attr("transform", "translate(" + parseInt(config.margin.left) + "," + (config.margin.top + config.padding.top) + ")");

        svg.yAxisSupport = svg.layers.axes.append("g")
            .attr('class', 'y-axis support')
            .attr("transform", "translate(" + (parseInt(config.margin.left) + 12) + "," + (config.margin.top + config.padding.top) + ")");
    }

    let redrawYAxis = function redrawYAxis(yScale,axes) {

        axes.yLinear = d3.axisLeft(yScale.linear);

        axes.yLinear
            .ticks(5);

        svg.yAxis
            .call(axes.yLinear);

    }

    let redrawYAxisStacked = function redrawYAxisStacked(scales,axes) {

        axes.yLinear = d3.axisLeft(scales.stacked);

        axes.yLinear
            .ticks(5);

        svg.yAxis
            .call(axes.yLinear);

    }

    let redrawYTimelineAxis = function redrawYTimelineAxis(scales,axes) {

        axes.yTimeline = d3.axisLeft(scales.timeline);

        axes.yTimeline
            .tickSizeOuter(0)
            .tickValues([]);

        axes.yTimelineSupport = d3.axisLeft(scales.timeline);

        axes.yTimelineSupport
            .ticks(d3.timeDay.every(1))
            .tickSize(10)
            .tickFormat( date => '');

        svg.yAxis
            .call(axes.yTimeline);


        svg.yAxisSupport
            .call(axes.yTimelineSupport);


    }


    return {
        drawXAxis : drawXAxis,
        redrawXBandAxis : redrawXBandAxis,
        redrawXTimeAxis : redrawXTimeAxis,
        drawYAxis : drawYAxis,
        redrawYAxis : redrawYAxis,
        redrawYAxisStacked : redrawYAxisStacked,
        redrawYTimelineAxis : redrawYTimelineAxis
    }
}

