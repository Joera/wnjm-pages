
class Youtube {

    constructor() {

        this.key = 'AIzaSyASEkTP1o1dLr-ieTzR4V5WaCI5TP5uOnY';
        this.channelslist = 'UU2_gAyN93xwM-Tj09GW-zZw';
        this.container = document.getElementById('youtube_wrapper');
    }


    init() {

        let self = this;
        let url = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails&maxResults=10&playlistId=' + this.channelslist + '&key=' + this.key;
        let html;

        var xhr = new XMLHttpRequest();
        xhr.open('GET',url);
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {

                    var data = JSON.parse(xhr.response);

               //     console.log(data);

                    for (let item of data.items) {

                        item.video_url = 'https://www.youtube.com/embed/' + item.contentDetails.videoId + '/?autoplay=1';
                        html = self.createHTML(item);
                        self.container.appendChild(html)
                    }
                }
            }
        }

    }

    createHTML(item) {

        let html;

        let a = document.createElement('a');
        a.href = 'https://www.youtube.com/watch?v=' + item.contentDetails.videoId;
        a.target = "_blank";
        a.classList.add('item');
        a.classList.add('youtube');

        let desc = document.createElement('span');
        desc.innerText = item.snippet.description;

        let svg = `<svg viewBox="0 0 120 120" preserveAspectRatio="xMinYMin slice" width="120" height="120" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                    <circle id="cirkel" r="53.75872" cy="61" cx="62" />
                    <polygon id="triangle" stroke-width="0" points="89.25741577148438,61.5 48.87129211425781,85.81138610839844 48.87129211425781,37.1886100769043 89.25741577148438,61.5 " strokeWidth="0" strokecolor="none" fill="#ffffff" edge="31.622777" orient="x" sides="3" shape="regularPoly" id="svg_6" cy="38" cx="42"/>
                    </svg>`;

        let still = document.createElement('img');
        if(item.snippet.thumbnails.maxres) {
            still.src = item.snippet.thumbnails.maxres.url;
        } else if (item.snippet.thumbnails.high) {
            still.src = item.snippet.thumbnails.high.url;
        }

        let play = document.createElement('div');
        play.classList.add('play-button');
        play.innerHTML = svg;

        let container = document.createElement('div');
        container.classList.add('yt_image_container');
        container.onclick = function() { console.log('play me') }

        container.appendChild(still);
        container.appendChild(play);

        
        a.appendChild(container);
        a.appendChild(desc);

        return a;
    }

    requestStats(video_id) {

        var url = 'https://www.googleapis.com/youtube/v3/videos?part=statistics&id=' + video_id + '&key=' + this.key;

        var xhr = new XMLHttpRequest();
        xhr.open('GET',url);
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {
                    return JSON.parse(xhr.response);
                }
            }
        }
    }
}


var youtube = new Youtube;
youtube.init();