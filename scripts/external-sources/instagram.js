class Instagram {

    constructor() {

        this.user_id = '325217584';
        this.client_id = 'e1407c2a7fdf46f1aa6bdce724801c29';
        this.token = '6164189089.1677ed0.db69bb506c2c47d5af5a2a27f89716c7';
        this.container = document.getElementById('instagram_wrapper');
    //    this.tag = 'noordzuidlijn';

    }


    init() {

        var self = this;
        var url = 'https://api.instagram.com/v1/users/self/media/recent/?client_id=' + this.client_id + '&access_token=' + this.token;
        var html;

        var xhr = new XMLHttpRequest();
        xhr.open('GET',url);
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {

                    var response = JSON.parse(xhr.response);

                    console.log(response);

                    for (let item of response.data) {

                        html = self.createHTML(item);
                        self.container.appendChild(html)
                    }
                }
            }
        }
    }

    createHTML(item) {

        let a = document.createElement('a');
        a.href = item.link;
        a.target = "_blank";
        a.classList.add('item');
        a.classList.add('instagram');

        let img = document.createElement('img');
        img.src = item.images.standard_resolution.url;

        let location = document.createElement('h3');
        location.classList.add('location');

        if(item.location) {
            location.innerText = item.location.name;
        }

        let caption = document.createElement('span');
        caption.innerHTML = item.caption.text;

        let likes = document.createElement('span');
        likes.classList.add('likes');
        likes.classList.add('smallerfont');
        likes.innerText = item.likes.count + ' vind-ik-leuks';

        a.appendChild(img);
        a.appendChild(location);
        a.appendChild(caption);
        a.appendChild(likes);

        return a;
    }
}


var instagram = new Instagram;
instagram.init();