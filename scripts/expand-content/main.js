{

    class Details {

        constructor() {

            var self = this;
            this.items = [].slice.call(document.querySelectorAll('#children-container .block'));

            this.DOM = {};

            const detailsTmpl = `
                <div class="details__bg details__bg--up"></div>
                <div class="details__bg details__bg--down">
                    <svg width="1" height="1" viewBox="0 0 1 1" preserveAspectRatio="none">
                        <defs>
                            <clipPath id="detail-clip-path" clipPathUnits="objectBoundingBox">
                                <polygon points="0.7 0, 1 0.2, 1 1, 0.3 1, 0 0.9, 0 0" />
                            </clipPath>
                            <clipPath id="image-clip-path" clipPathUnits="objectBoundingBox">
                                <polygon points="0.5 0, 1 0, 1 1, 0.5 1, 0 0.95, 0 0" />
                            </clipPath>
                        </defs>
                        <rect x="0" y="0" width="1" height="1" clip-path="url(#detail-clip-path)"></rect>
                    </svg>
                </div>
                <img class="details__img" src="" alt="img 01"/>
                
                
                <div class="details__scrolltainer">
                    <h2 class="details__title"></h2>
                    <p class="details__description"></p>
                </div>
                <button class="details__close"><svg class="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
                        <path fill="#000000" d="M10.707 10.5l5.646-5.646c0.195-0.195 0.195-0.512 0-0.707s-0.512-0.195-0.707 0l-5.646 5.646-5.646-5.646c-0.195-0.195-0.512-0.195-0.707 0s-0.195 0.512 0 0.707l5.646 5.646-5.646 5.646c-0.195 0.195-0.195 0.512 0 0.707 0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146l5.646-5.646 5.646 5.646c0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146c0.195-0.195 0.195-0.512 0-0.707l-5.646-5.646z"></path>
                    </svg></button>
                `;

            this.DOM.details = document.createElement('div');
            this.DOM.details.className = 'details';
            this.DOM.details.innerHTML = detailsTmpl;
            DOM.content.appendChild(this.DOM.details);
            this.init();
        }
        init() {
            this.DOM.newBgUp = this.DOM.details.querySelector('.details__bg--up');
            this.DOM.newBgDown = this.DOM.details.querySelector('.details__bg--down');
            this.DOM.newImage = this.DOM.details.querySelector('.details__img');
            this.DOM.title = this.DOM.details.querySelector('.details__title');
            this.DOM.scrolltainer = this.DOM.details.querySelector('.details__scrolltainer');
            // this.DOM.deco = this.DOM.details.querySelector('.details__deco');
            // this.DOM.subtitle = this.DOM.details.querySelector('.details__subtitle');
            this.DOM.text = this.DOM.details.querySelector('.details__description');
            // this.DOM.cart = this.DOM.details.querySelector('.details__addtocart');
            this.DOM.close = this.DOM.details.querySelector('.details__close');
            // this.DOM.magnifier = this.DOM.details.querySelector('.details__magnifier');

            this.initEvents();
        }
        initEvents() {
            this.DOM.close.addEventListener('click', () => this.close());
        }
        fill(info) {
            this.DOM.newImage.src = info.img;
            this.DOM.title.innerHTML = info.title;
            this.DOM.text.innerHTML = info.text;
        }
        getProductDetailsRect() {
            return {
                originalBgRect: this.DOM.originalBg.getBoundingClientRect(),
                newBgRect: this.DOM.newBgDown.getBoundingClientRect(),
                originalImageRect: this.DOM.originalImage.getBoundingClientRect(),
                newImageRect: this.DOM.newImage.getBoundingClientRect()
            };
        }
        open(data) {

            if(window.location.href.indexOf('?block=') < 0) {
                window.history.pushState(null, null, window.location.href + '?block=' + data.slug);
            }

            if (this.isAnimating) return false;
            this.isAnimating = true;

            this.DOM.details.classList.add('details--open');

            this.DOM.originalBg = data.background;
            this.DOM.originalImage = data.image;

            this.DOM.originalBg.style.opacity = 0;
            this.DOM.originalImage.style.opacity = 0;

            const rect = this.getProductDetailsRect();

            this.DOM.newBgDown.style.transform = `translateX(${rect.originalBgRect.left-rect.newBgRect.left}px) translateY(${rect.originalBgRect.top-rect.newBgRect.top}px) scaleX(${rect.originalBgRect.width/rect.newBgRect.width}) scaleY(${rect.originalBgRect.height/rect.newBgRect.height})`;

            this.DOM.newBgDown.style.opacity = 1;

            this.DOM.newImage.style.transform = `translateX(${rect.originalImageRect.left-rect.newImageRect.left}px) translateY(${rect.originalImageRect.top-rect.newImageRect.top}px) scaleX(${rect.originalImageRect.width/rect.newImageRect.width}) scaleY(${rect.originalImageRect.height/rect.newImageRect.height})`;
            this.DOM.newImage.style.opacity = 1;

            this.DOM.title.style.opacity = 1;
            this.DOM.scrolltainer.style.opacity = 1;
            this.DOM.close.style.opacity = 1;

            anime({
                targets: [this.DOM.newBgDown],
                duration: (target, index) => index ? 800 : 250,
                easing: (target, index) => index ? 'easeOutElastic' : 'easeOutSine',
                elasticity: 250,
                translateX: 0,
                translateY: 0,
                scaleX: 1,
                scaleY: 1,
                complete: () => this.isAnimating = false
            });

            anime({
                targets: [this.DOM.newImage],
                duration: 1000,
                offset: 500,
                easing: (target, index) => index ? 'easeOutElastic' : 'easeOutSine',
                elasticity: 250,
                translateX: [-1000,0],
                translateY: 0,
                scaleX: 1,
                scaleY: 1,
                complete: () => this.isAnimating = false
            });

            anime({
                targets: [this.DOM.text],
                duration: 1000,
                offset: 1000,
                easing: 'easeOutExpo',
                translateY: [1000,0],
                opacity: 1
            });
        }

        close() {

            window.history.pushState(null, null,window.location.pathname)
            if (this.isAnimating) return false;
            this.isAnimating = true;

            this.DOM.details.classList.remove('details--open');

            this.DOM.newBgDown.style.opacity = 0;
            this.DOM.originalBg.style.opacity = 1;

            this.DOM.newImage.style.opacity = 0;
            this.DOM.originalImage.style.opacity = 1;

            this.DOM.text.style.opacity = 0;
            this.DOM.title.style.opacity = 0;
            this.DOM.scrolltainer.style.opacity = 0;
            this.DOM.close.style.opacity = 0;

            this.isAnimating = false;
        }

    };

    class Item {

        constructor(el) {
            this.DOM = {};
            this.DOM.el = el;
            this.DOM.title = this.DOM.el.querySelector('.overlay h3');
            this.DOM.background = this.DOM.el.querySelector('.overlay');
            this.DOM.image = this.DOM.el.querySelector('.img-container img');
            this.DOM.text = this.DOM.el.querySelector('.expanding-block .text');
            this.DOM.slug = this.DOM.title.className;

            this.info = {
                img: this.DOM.image.getAttribute("data-src"),
                title: this.DOM.title.innerHTML.replace(/(<br>)*/g,""),
                text: this.DOM.text.innerHTML
            };

            this.initEvents();
        }

        initEvents() {
            let self = this;
            this.DOM.el.addEventListener('click', () => self.open());

            if(window.location.search) {

                var searchParam = window.location.search.split('=')[1];

                if(this.DOM.slug === searchParam) {
                    self.open();
                }
            }
        }

        open() {

            DOM.details.fill(this.info);
            DOM.details.open({
                background: this.DOM.background,
                image: this.DOM.image,
                text: this.DOM.text,
                slug: this.DOM.slug
            });
        }
    }
    const DOM = {};
    DOM.grid = document.querySelector('#children-container');
   // && !detect.ie()
    if(DOM.grid) {

        DOM.content = DOM.grid.parentNode;
        // DOM.hamburger = document.querySelector('.dummy-menu');
        DOM.gridItems = [].slice.call(DOM.grid.querySelectorAll('.block'));
        let items = [];
        DOM.details = new Details();
        DOM.gridItems.forEach(item => items.push(new Item(item)));
    }
}

