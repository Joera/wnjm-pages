var SearchResultInfo = function SearchResultInfo  (language) {

    var addHTML = function addHTML(content, pageIndex, container) {

        let html;

        if (content.hits.length < 1) {
            html = (language === 'en') ? 'Your search returned no results' : 'Uw zoekopdracht heeft geen resultaten opgeleverd.';
        } else {
            let beginpunt = (parseInt(pageIndex) * content.hitsPerPage + 1);
            let eindpunt;
            if (content.nbHits > content.hitsPerPage * (parseInt(pageIndex) + 1)) {
                eindpunt = content.hitsPerPage * (parseInt(pageIndex) + 1);
            } else {
                eindpunt = content.nbHits;
            }
            if(language === 'en') {
                html = beginpunt + ' - ' + eindpunt + ' of ' + content.nbHits + ' results';
            } else {
                html = beginpunt + ' - ' + eindpunt + ' van ' + content.nbHits + ' resultaten';
            }

        }
        container.innerHTML = html;
        container.style.display = 'block';
    }

    return {
        addHTML: addHTML
    };

}