
var SearchCheckboxes = function SearchCheckboxes(elements) {

 //
    var _slugify = function _slugify(string) {
        const a = 'àáäâãåăæçèéëêǵḧìíïîḿńǹñòóöôœøṕŕßśșțùúüûǘẃẍÿź·/_,:;'
        const b = 'aaaaaaaaceeeeghiiiimnnnooooooprssstuuuuuwxyz------'
        const p = new RegExp(a.split('').join('|'), 'g')
        return string.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
            .replace(/&/g, '-and-') // Replace & with ‘and’
            .replace(/[^\w\-]+/g, '') // Remove all non-word characters
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, '') // Trim - from end of text
    }

    var getCheckboxes = function getCheckboxes(facet) {

        return [].slice.call(elements.filterContainer.querySelectorAll('li > div > input[type="checkbox"][data-facet="' + facet + '"'));
    }

    var setListeners = function setListeners(facet) {

        let checkboxes = getCheckboxes(facet);

        for (let box of checkboxes) {

                box.addEventListener('change', function (e) {
                    e.stopPropagation();
                    search.onFilterChange(facet,_readCheckboxes(checkboxes))

                }, false);

        }
    }

    var _readCheckboxes = function _readCheckboxes(checkboxes) {

        let array = [];
        checkboxes.forEach( function(el,i) {
            array.push({
                    id: el.id.substring(7),  // remove option_
                    state: el.checked
                });
        });

        return array;
    }


    var setCheckboxes = function _setCheckboxes(facet,list) {
        
        let array = list.map(
            function(t) {
                if(t.state) {
                    return t.id;
                }
            }
        );

        let checkboxes = getCheckboxes(facet);

        checkboxes.forEach( (el) => {
            if(array.indexOf(el.id.substring(7)) > -1) {
                el.checked = true;
            } else {
                el.checked = false;
            }
        });
    }

    var _initCheckboxes = function _initCheckboxes() {

        types.forEach( (el) => {

            el.checked = true;
            let item = {
                id: el.id.substring(7),
                state: true
            };
            this.filter.types.push(item)
        });



    }

    var createCheckboxes = function createCheckboxes(list,options,facet) {

        let li;

        let ul = document.createElement('ul');
        ul.classList.add('checkboxes_list');

            options.forEach( function(item) {

                li = document.createElement('li');
                li.classList.add('checkbox_option');
                li.innerHTML = '<div class="checkbox"><input id="option_' + _slugify(item) +  '" type="checkbox" data-facet="' + facet + '" /><label for="option_' + _slugify(item) + '"></label></div><span class="smallerfont">' + item + '</span>';
                //   env_li .onclick = function() { search.onFilterChange('render_environments',item.slug); filteredByTag = true; };
                ul.appendChild(li);
            });

            let div = document.createElement('div');
            let h = document.createElement('span');
            h.classList.add('title','taxonomy');
            div.appendChild(h);
            div.appendChild(ul);
            elements.filterContainer.appendChild(div);


        setListeners(facet);
    }

    var _ObjectToArray = function _ObjectToArray(object) {

        let array = [],
            v;

        for (var key in object) {
            if (object.hasOwnProperty(key)) {
                let v = {
                    'name' : key,
                    'count' : object[key]
                }
                array.push(v);
            }
        }
        return array;
    }

    return {
        setListeners : setListeners,
        setCheckboxes : setCheckboxes,
        createCheckboxes : createCheckboxes
    };

}