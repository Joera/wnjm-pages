class ScrollMethods {

    constructor() {
    }

    init() {

        var self = this;
        this.navigationMapContainer = document.getElementById('navigation-map-container');
        this.header = document.getElementById('header');

        if (window.innerWidth > 760) {

            let scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
            // if (scrollPosition < 100) {
            //     if (self.navigationMapContainer) { self.navigationMapContainer.classList.add('hoversize'); }
            // }

            window.onscroll = function () {
                scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
                if (scrollPosition > window.innerHeight) {
                    self.header.classList.add('sticky');
                } else {
                    self.header.classList.remove('sticky');
                }
                if (scrollPosition > 100) {  // (window.innerHeight / 6)
                    if (self.navigationMapContainer && window.innerWidth > 760) { self.navigationMapContainer.classList.remove('hoversize'); }
                } else {
                    if (self.navigationMapContainer && window.innerWidth > 760) { self.navigationMapContainer.classList.add('hoversize'); }
                }
            };
        }
    }
}

var scrollMethods = new ScrollMethods;
scrollMethods.init();