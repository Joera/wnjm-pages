class Storytelling {

	constructor() { 

		this.toc = document.getElementById('inhoudsopgave_js'); 
	}

	init() {

		var self = this;

		window.onscroll = function() {

			// duplicate header functionality .. want mag maar een keer window.scroll op pagina
			var header = document.getElementById('header'); 
    
		    var scrollPosition = window.pageYOffset || document.documentElement.scrollTop; 

		    if (scrollPosition > 50) { 

		    	header.classList.add('condensed');
		    
		    } else {

		    	header.classList.remove('condensed'); 
		    }

		    if (scrollPosition > 800) { 

		    	if (self.toc !== null) {  self.toc.classList.add('visible'); } 
		    
		    } else {

		    	if (self.toc !== null) {  self.toc.classList.remove('visible'); }
		    }
		};
	}

	showTOC() {

		if (this.toc !== null) { this.toc.classList.add('showcontents'); } 
	}

	hideTOC() {

		if (this.toc !== null) { this.toc.classList.remove('showcontents'); }
	}
}

var storytelling = new Storytelling;
storytelling.init(); 