class References {

	constructor(){

		this._referencesGroup = null; 
	    this._geel = '#fcca32';
	    this._oranje = '#f79234';                                   
	    this._lichtblauw = '#199ECA';
	    this._grijs = '#D7D7D7';
	    this._donkergrijs = '#646464';
	    this._now = moment();
	    this._dataset = null;
	}

	draw(config,dataset) {

        this._dataset = dataset;

		config.yScale = d3.scaleLinear().domain([config.yExtent[0],config.yExtent[1]]).range([0,config.height]);
      	config.xScale = d3.scaleTime().range([0,config.width]).domain([config.start,config.einde]);

		if (this._referencesGroup === null) { 

        	this._referencesGroup = config.mainGroup.append('g')
            	.attr('class', 'references');
		}

      	var referenceGroup = this._referencesGroup.selectAll('.referencePoint')
			.data(this._dataset); // .filter(function (d) { return d.class.indexOf('gantt_item') > -1; }));

		referenceGroup
		.enter().append('image')
            .attr('class','referencePoint')
            .attr('xlink:href',function (d) { 

                if (d.reference) {  return '/assets/icons/post-icon.svg?v=4'; } 
                else { return '/assets/icons/lnr-question-circle.svg?v=4'; }
            })
            .attr('height', 23)
            .attr('width', 13)  
            .attr('y', function (d) { return config.yScale(d.position) + 0.31 * config.yScale(1) - 14; })
            .style('fill',"#fff")
            .style('stroke','#fff')
            
        .merge(referenceGroup)
        	.style('opacity',0)
        	.attr('x', function (d) {

				if (d.endType === false) { 

                	return config.xScale(moment(d.from)) + 10;

				} else { 

					return config.xScale(moment(d.to)) - 20;
				}
            })
			.transition()
			.duration(200)
			.delay(1000)
			.style('opacity',1)
			// .on('click', function(d,i){  scope.config.toggle(d); });
      		;

      	referenceGroup.exit().remove();
	}
}

var references = new References(); 