class Mask {

	constructor() {

    	this._now = moment();
    	this._maskedElements = null; 
    	this._dataset = null;
	}

	init(config,dataset) {

		this._maskedElements = config.mainGroup.append('g')
			.attr('class', 'mask');

		this.clipRect = config.clipPath.append('rect')     
			.attr("height", 400)      
			.attr("x", 0)         
			.attr("y", 0)
        ;

		this._dataset = dataset;

        this._dataset = this._dataset.filter(function(d) {
            if (d.class !== 'milestone') {  return d; }
        });
	}

	draw(config) {

		config.yScale = d3.scaleLinear().domain([config.yExtent[0],config.yExtent[1]]).range([0,config.height]);
        config.xScale = d3.scaleTime().range([0,config.width]).domain([config.start,config.einde]);
        
        this.clipRect    
			.attr("width", config.xScale(this._now) + 0.5)
        ;
		
        // this.background(config);
		this.rectangles(config);

	}

    background(config) {

        var self = this; 

        this._maskedElements.append('rect')
            .attr('class', 'background_passed')
            .attr('clip-path', 'url(#finished)') 
            .attr('height', config.height + 20)
            .attr('width', config.width)
            .attr('x', 0)
            .attr('y', 0)
            .style('fill', lichtgrijs)
            .style('opacity', .3)
            
            ;
    }

	rectangles(config) { 

		var self = this;

		var rectangles = this._maskedElements.selectAll('.masked')
                .data(this._dataset);

        rectangles
        .enter()
            .append('rect')
            .attr('class', 'masked')
            .attr('clip-path', 'url(#finished)') // .attr("clip-path", "url(" + self._config.path + "#finished)")
            .attr('height', function (d) { return 0.25 * config.yScale(1); })
            .attr('width', 0)
            .attr('x', 0)
            .attr('y', function (d) { return config.yScale(d.position) + 0.45 * config.yScale(1) + 3;  })  //
            .style('fill', function (d) { return grijs; })
        .merge(rectangles)
            .attr('width', function (d) { let width = config.xScale(moment(d.to)) - config.xScale(moment(d.from)); return width > 0 ? width : 0; })
            .attr('x', function (d) { return config.xScale(moment(d.from)); })
            ;

        rectangles.exit().remove();
    }
}

var mask = new Mask(); 