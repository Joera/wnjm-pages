

class NewsletterForm {

    constructor() {

        this.hasErrors = true;
    }

    init() {

        var self = this;

        this.forms = [].slice.call(document.querySelectorAll('form.newsletter'));
        this.form = false;
        let email;
        this.message = {};

        let listen = function(event) {

            event.preventDefault();
            email = event.target.querySelector('input[type=email]').value;
            if(self.validate(email)) {
                self.submit(email,event.target);
                event.target.removeEventListener("submit",listen, false);
                event.target.querySelector('input[type=submit]').disabled = true;
            }
        }

        if(this.forms) {
            for (let form of this.forms) {
                form.addEventListener("submit", listen, false);
            }
        }
    }

    validate(email) {

        if (email.length > 0 && this.validateEmail(email)) {
            return true;
        } else {
            return false;
        }
    }

    submit(email,form) {


        let self = this;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', WP_URL + '/wp-json/wp/v2/form?type=newsletter&env=uithoornlijn&email=' + email);
        xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4; // readyState 4 means the request is done.
            var OK = 200; // status 200 is a successful return.
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {
                    console.log(xhr);
                    form.querySelector('span.message').textContent = "Uw aanvraag is succesvol verzonden.";
                    form.querySelector('input').value = "";

                } else {
                    form.querySelector('span.message').textContent = "De website heeft geen verbinding kunnen maken met de server.";

                }
            }
        };
    }

    validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

}

var newsletterForm = new NewsletterForm;
newsletterForm.init();