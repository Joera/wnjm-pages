const sm = 900;
const xxl  = 1900;


class HomeMenu {

    constructor() {}

    init() {

        // bij xxl multiple active containers

        let self = this;



        this.homeMenu = document.getElementById('dashboard-menu');
        this.dashboard = document.querySelector('#dashboard');
        this.containers = [].slice.call(this.dashboard.children).filter ( c => c.hasAttribute('data-option'));
        this.menuItems = [].slice.call(this.dashboard.querySelectorAll('li:not(.active-background)'));
        this.moreLinks = [].slice.call(this.dashboard.querySelectorAll('a.more-link'))


        this.setMenuListeners();

        // // when dom is rendered
        window.addEventListener("load", function(event) {

            // add a perceived delay for smoothness
            setTimeout( () => {

                self.containers = self.containers.filter( c => c.hasAttribute('data-option'));
                self.open('comments');

            },500)
        });
    }

    setMenuListeners() {

        let self = this;

        let menuListener = event => {
            var targetElement = event.target || event.srcElement;
            self.open(self._getOptionFromEl(targetElement))
        }

        for (let menuItem of this.menuItems) {
            menuItem.addEventListener('click', menuListener, false)
        }
    }

    _getOptionFromEl(elem) {

        if (!elem.tagName || elem.tagName === undefined) {
            return elem
        } else {
            for (; elem && elem !== document; elem = elem.parentNode) {
                if (elem.tagName === 'LI') {
                    return elem.getAttribute('data-option');
                }
            }
        }
    }

    open(option) {  // alleen waarde doorgeven? en dan ook een array met waardes?

        let self = this;

        for (let container of this.containers) {

            container.style.height = 0;

            if(container.getAttribute('data-option') === option) {

               container.style.height = 'calc(100% - 1.5rem)';
            }
        }

        let active = self.containers.filter((c) => {
            return c.getAttribute('data-option') === option;
        })[0];

        for (let link of this.moreLinks) {

            link.style.display = 'none';

            if (link.getAttribute('data-link') === option) {

                link.style.display = 'inline';
            }
        }

        for (let menuItem of this.menuItems) {

            menuItem.classList.remove('active');

            if(menuItem.getAttribute('data-option') === option) {

                menuItem.classList.add('active');

            }
        }
    }
}

var dashboardMenu = new HomeMenu;
dashboardMenu.init();