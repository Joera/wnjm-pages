class VideoLoop {

    constructor() {}

    init() {


        this.video = document.getElementsByTagName('video')[0];
        this.source = this.video.getElementsByTagName('source')[0];

        if (window.innerWidth > 1200) {

            this.source.src = 'https://wijnemenjemee.nl/assets/img/videoloop.mp4';
        }
    }
}

var videoLoop = new VideoLoop;
videoLoop.init();