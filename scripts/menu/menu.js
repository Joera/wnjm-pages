class Menu {

    constructor() {}

    init() {


        this.openMenuButton = document.getElementById('open-menu');
        this.closeMenuButton = document.getElementById('close-menu');
        this.header = document.getElementById('menu');
        this.nav = document.getElementById('nav');

        this.body = document.getElementsByTagName("body")[0];
        this.searchInput = document.getElementById('search-input');

        this.language = document.documentElement.lang;

        this.addOpenMenuListener();
    }

    addOpenMenuListener() {

        var self = this;

        let listener = event => {

            event.preventDefault();
            self.open();
            event.target.removeEventListener("click",listener, false);
        }

        if(this.openMenuButton) {
            this.openMenuButton.addEventListener('click', listener, false)
        }

    }

    addCloseMenuListener() {

        var self = this;

        let listenerClose = event => {
            event.preventDefault();
            self.close();
            event.target.removeEventListener("click",listenerClose, false);
        }

        if(this.closeMenuButton) {
            this.closeMenuButton.addEventListener('click', listenerClose, false)
        }
    }

    open() {

        let self = this;

        this.nav.style.display = "flex";
        this.openMenuButton.style.display = "none";

        // let headerHeightOpen = window.innerHeight - 35;

        anime.timeline()
            .add({
                targets: self.nav,
                translateX: [500,0],
                easing: 'easeOutExpo',
                duration: 250,
                begin: function() {
                    self.closeMenuButton.style.display = "flex";
                    self.addCloseMenuListener();
                },
                complete: function() {
                    self.nav.style.display = 'flex';
                }
            })
            // .add({
            //     targets: self.navItems,
            //     opacity: [0,1],
            //     easing: 'easeInSine',
            //     duration: 20,
            //     // delay: anime.stagger(100, {start : 0})
            // })
            // .add({
            //     targets: self.nav,
            //     opacity: [0,1],
            //     easing: 'easeInSine',
            //     duration: 250,
            // })
            // .add({
            //     targets: self.navBanners,
            //     opacity: [0,1],
            //     easing: 'easeInSine',
            //     // duration: 1000,
            //     delay: anime.stagger(200, {start : 0})
            // })

        ;

    }

    close() {

        const self = this;
        self.closeMenuButton.style.display = "none";

        anime.timeline()
            .add({
                targets: self.nav,
                translateX: [0,1500],
                easing: 'easeInSine',
                duration: 250,
                begin: function() {
                    self.openMenuButton.style.display = "flex";
                    self.addOpenMenuListener();
                }
            })


        ;
    }

    onEnterSearch(e) {
        let self = this,
            key = e.which || e.keyCode;
        if(key == 13) {

            let renderEnv = 'wijnemenjemee';

            self.search(renderEnv);
        }
    }

    search(renderEnv) {

        if (this.language === 'en') {
            window.location.href = '/zoeken/?query=' + this.searchInput.value + '&filter[env]=' + renderEnv;
        } else {
            window.location.href = '/zoeken/?query=' + this.searchInput.value  + '&filter[env]=' + renderEnv;
        }

    }

    // openInput() {
    //
    //
    //     document.querySelector('#search-box').classList.add('active');
    //     document.querySelector('#search-box #search-input').focus();
    //     let button = document.querySelector('#search-box button');
    //
    //     setTimeout( function() {
    //         button.setAttribute('onclick','menu.search()');
    //     },200)
    // }
}

var menu = new Menu;
menu.init();