class PortretVideos {

    constructor() {}

    init() {


        this.video = document.getElementsByTagName('video')[0];
        this.source = this.video.getElementsByTagName('source')[0];

        let sourceSrc =  this.source.src;

        if (window.innerWidth > 1200 && sourceSrc.indexOf('-mobile')) {

            this.source.src = sourceSrc.replace('-mobile','');
        }
    }
}

var portretVideos = new PortretVideos;
portretVideos.init();