class Slideshow {

    constructor() {

        this.slides = [].slice.call(document.querySelectorAll('.banner-homepage-slide'));
    }

    init() {

        let i = 0;
        setInterval( () => {

            for (let slide of this.slides) {
                slide.classList.remove('active');
            }

            this.slides[i].classList.add('active');

            (i < 2) ? i++ : i = 0;

        },6000);
    }
}

var slideshow = new Slideshow;
slideshow.init();
