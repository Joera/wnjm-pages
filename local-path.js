/**
 * Central store for all paths that are used in tha application
 */
module.exports = function() {

    return {
        projectFolder: '/opt/sg-core/pages',
        distFolder: '/var/www/vhosts/wijnemenjemee.nl/httpdocs/',
        productionFolder: '/var/www/vhosts/wijnemenjemee.nl/httpdocs',
        htmlRoot: '/var/www/vhosts/wijnemenjemee.nl/httpdocs/'
    }
};
