"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function ieVersion() {
    var ua = window.navigator.userAgent;
    if (ua.indexOf("Trident/7.0") > 0) return 11;else if (ua.indexOf("Trident/6.0") > 0) return 10;else if (ua.indexOf("Trident/5.0") > 0) return 9;else return 0; // not IE9, 10 or 11
}

!window.ActiveXObject && "ActiveXObject";
function isIE11() {
    return !!navigator.userAgent.match(/Trident.*rv[ :]*11\./);
}

function isWebkit() {
    if (!!window.webkitURL) {
        return true;
    } else {
        return false;
    }
}

function isIos() {
    if (['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0) {
        return true;
    } else {
        return false;
    }
}

function isIpad() {
    if (['iPad'].indexOf(navigator.platform) >= 0) {
        return true;
    } else {
        return false;
    }
}

function isRetina() {
    if (window.devicePixelRatio > 1) {
        return true;
    } else {
        return false;
    }
}
//
// var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
// var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

// dit is een nieuwe //

function webgl_detect(return_context) {
    if (!!window.WebGLRenderingContext) {
        var canvas = document.createElement("canvas"),
            names = ["webgl", "experimental-webgl", "moz-webgl", "webkit-3d"],
            context = false;

        for (var i = 0; i < 4; i++) {
            try {
                context = canvas.getContext(names[i]);
                if (context && typeof context.getParameter == "function") {
                    // WebGL is enabled
                    if (return_context) {
                        // return WebGL object if the function's argument is present
                        return { name: names[i], gl: context };
                    }
                    // else, return just true
                    return true;
                }
            } catch (e) {}
        }

        if (canvas && canvas !== null) {
            canvas.remove();
        }

        // WebGL is supported, but disabled
        return false;
    }

    // WebGL not supported
    return false;
}
var lichtgrijs = 'rgb(230,230,230)';
var grijs = 'rgb(220,220,220)';
var donkergrijs = 'rgb(100,100,100)';
var zwart = 'rgb(51,51,51)';
var wit = 'rgb(255,255,255)';

var lichtgeel = 'rgb(255,224,99)';
var geel = 'rgb(255,203,0)';
var donkergeel = 'rgb(248,175,0)';
var lichtoranje = 'rgb(242,145,0)';
var oranje = 'rgb(237,114,3)';
var lichtrood = 'rgb(231,78,15)';
var rood = 'rgb(229,50,18)';
var donkerrood = 'rgb(209,10,17)';
var lichtblauw = 'rgb(156,198,212)';
var blauw = 'rgb(0,154,200)';
var donkerblauw = 'rgb(0,111,157)';
var navy = 'rgb(0,85,120)';
function loadJSON(filepath, callback) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', filepath, true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

// for babel
// Array.includes = function() {
//     let [first, ...rest] = arguments;
//     return Array.prototype.includes.apply(first, rest);
// }
// // for IE
// if (!String.prototype.includes) {
//     String.prototype.includes = function() {
//         'use strict';
//         return String.prototype.indexOf.apply(this, arguments) !== -1;
//     };
// }

var ActiveMarkers = function () {
    function ActiveMarkers(map, config, popup, center) {
        _classCallCheck(this, ActiveMarkers);

        this._map = map;
        this._config = config;
        this._interactionPopup = popup;
        this._center = center;
    }

    ActiveMarkers.prototype.setGL = function setGL(array) {

        var self = this,
            filter = void 0;

        if (array && array.length > 0 && array.length < 2) {
            // < 2 zorgt ervoor dat ie op homepage niet actief is
            filter = ['in', 'slug'];
            array.forEach(function (item) {
                filter.push(item);
            });
            // voor verzamelpagina's zoals homepage .. nu even niks
        } else if (array && array.length > 1) {
            filter = ["==", 'slug', ''];
            // als activeItems leeg is zoals bijv bij bouwprojecten
        } else {
            filter = ['has', 'slug'];
        }

        if (self._map.getLayer('icons-active')) {

            self._map.setFilter('icons-active', filter);

            setTimeout(function () {
                self._center.setGL();
            }, 2000);
        }
    };

    ActiveMarkers.prototype.setJS = function setJS(content, activeItems) {

        var self = this,
            m = void 0;

        if (activeItems && activeItems.length < 2) {

            activeItems.forEach(function (item) {
                m = document.querySelector('.leaflet-marker-icon.' + item);
                // console.log(m);
                if (m) {
                    m.classList.add('active');
                }
            });

            self._center.setJS(content, activeItems);
        }
    };

    return ActiveMarkers;
}();

var Center = function () {
    function Center(map, config) {
        _classCallCheck(this, Center);

        this._map = map;
        this._config = config;
    }

    Center.prototype.setGL = function setGL() {

        var self = this,
            activeFeatures = self._map.queryRenderedFeatures({ layers: ['icons-active'] });

        if (activeFeatures && activeFeatures.length === 1) {
            var latLng = [activeFeatures[0].geometry.coordinates[0], activeFeatures[0].geometry.coordinates[1]];
            self._map.setCenter(latLng);
            self._map.setZoom(self._config.zoom);
        } else if (activeFeatures && activeFeatures.length > 1) {

            var collection = activeFeatures.map(function (f) {
                return f.geometry.coordinates;
            });
            // self._map.fitBounds(collection, {
            //     padding: {top: 10, bottom:10, left: 10, right: 10}
            // });
        }
    };

    Center.prototype.setJS = function setJS(content, activeItems) {

        var self = this,
            latLng = void 0,
            activeMarkers = void 0;

        // console.log(activeItems);

        if (activeItems && activeItems.length < 2) {

            activeMarkers = content.features.filter(function (f) {
                return f.properties.slug === activeItems[0];
            });

            // console.log(activeMarkers);

            if (activeMarkers[0]) {
                latLng = new L.LatLng(parseFloat(activeMarkers[0].geometry.coordinates[1]) + 0, parseFloat(activeMarkers[0].geometry.coordinates[0]) - 0);
                setTimeout(function () {
                    self._map.setView(latLng, self._config.zoom);
                }, 500);
            }
        } else {
            // self._map.fitBounds(activeMarkers.getBounds().pad(2.5));
        }
    };

    return Center;
}();

var ContentLayer = function () {
    function ContentLayer(map, config, popup, activeItems) {
        _classCallCheck(this, ContentLayer);

        this._map = map;
        this._config = config;
        this.interactionPopup = popup;
        this.activeItems = activeItems;
    }

    ContentLayer.prototype.draw = function draw(content) {

        var self = this,
            html = void 0,
            lngLat = void 0,
            type = void 0,
            offset = void 0;

        if (webgl_detect()) {

            self._map.addSource('markers', {
                'type': 'geojson',
                'data': content
            });

            self._map.addLayer({
                "id": "icons",
                "type": "symbol",
                "source": "markers",

                "layout": {
                    "icon-image": {
                        property: 'type',
                        type: 'categorical',
                        stops: [['project', "projectIcon"], ['blog', "blogIcon"]]
                    },
                    "icon-padding": 0,
                    "icon-allow-overlap": true,
                    "icon-anchor": 'bottom',
                    "icon-offset": [20, 10],
                    "visibility": "visible"

                    // "icon-rotate": 25
                },
                "paint": {
                    "icon-opacity": 1
                },
                "filter": ['any', ['==', 'type', 'project'], ['==', 'type', 'blog']]
            });

            if (self._config.highlights) {

                self._map.addLayer({
                    "id": "icons-active",
                    "type": "symbol",
                    "source": "markers",

                    "layout": {
                        "icon-image": {
                            property: 'type',
                            type: 'categorical',
                            stops: [['project', "projectIconActive"], ['blog', "blogIconActive"]]
                        },
                        "icon-padding": 0,
                        "icon-allow-overlap": true,
                        "icon-anchor": 'bottom',
                        "icon-offset": [15, 10],
                        "visibility": "visible",
                        "icon-size": 1
                        // "icon-rotate": 25
                    },
                    "paint": {
                        "icon-color": "#000"

                    },
                    "filter": ['all', ["==", 'slug', '']]
                });
            }

            self._map.on("click", "icons", function (e) {

                self._map.getCanvas().style.cursor = 'pointer';

                html = self.interactionPopup.createPopup(e.features[0]);
                offset = [18, -42];
                lngLat = e.features[0].geometry.coordinates;
                type = e.features[0].properties.type;

                self.interactionPopup.openPopup(self._map, html, lngLat, type, offset);
            });

            self._map.on("click", "icons-active", function (e) {

                self._map.getCanvas().style.cursor = 'pointer';

                html = self.interactionPopup.createPopup(e.features[0]);
                offset = [16, -34];
                lngLat = e.features[0].geometry.coordinates;
                type = e.features[0].properties.type;

                self.interactionPopup.openPopup(self._map, html, lngLat, type, offset);
            });

            self.activeItems.setGL(self._config.activeItems);
        } else {

            var marker = void 0,
                markers = [],
                _html = void 0,
                latLng = void 0,
                popup = void 0,
                initialPopups = void 0,
                blogIcon = void 0;

            var iconGenerator = new IconGenerator();

            content.features.forEach(function (item) {

                if (item.properties.type === 'blog') {
                    blogIcon = iconGenerator.blogIcon(item);
                } else if (item.properties.type === 'project') {
                    blogIcon = iconGenerator.projectIcon(item);
                }

                marker = L.marker([item.geometry.coordinates[1], item.geometry.coordinates[0]], { icon: blogIcon }); // .bindPopup(item.title);

                // console.log(item);
                // console.log(marker);

                marker.on('click', function (e) {

                    _html = self.interactionPopup.createPopup(item);
                    //open popup;
                    initialPopups = L.popup({
                        className: item.properties.type,
                        offset: [0, 36]
                    }).setLatLng(e.latlng).setContent(_html).openOn(self._map);
                });
                markers.push(marker);
            });

            this.markers = new L.featureGroup(markers);
            this.markers.addTo(self._map);

            self.activeItems.setJS(content, self._config.activeItems);
            self._map.fitBounds(self.markers.getBounds().pad(1));
        }
    };

    // voor bereikbaarheid


    ContentLayer.prototype.zoom = function zoom() {

        var self = this,
            coordinates = self._config.dataset.lines.features[1].geometry.coordinates,
            bounds = coordinates.reduce(function (bounds, coord) {
                return bounds.extend(coord);
            }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

        self._map.fitBounds(bounds, {
            padding: 100
        });
    };

    // voor bereikbaarheid


    ContentLayer.prototype.addLegend = function addLegend() {

        var legend = document.createElement('div');
        legend.classList.add('legend');
        self.hostContainer.appendChild(legend);
    };

    ContentLayer.prototype.navigate = function navigate(url) {

        window.location.href = url;
    };

    return ContentLayer;
}();

var IconGenerator = function () {
    function IconGenerator() {
        _classCallCheck(this, IconGenerator);
    }

    IconGenerator.prototype.blogIcon = function blogIcon(item) {

        return L.divIcon({

            iconSize: (45, 63),
            className: 'blog yellow ' + item.properties.slug,
            iconAnchor: (0, 0),
            popupAnchor: (0, 0),
            html: "\n\n               \n                        <svg width=\"53px\" height=\"46px\" viewBox=\"0 0 53 46\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n                            <defs>\n                                <linearGradient x1=\"63.6706155%\" y1=\"0%\" x2=\"77.9408035%\" y2=\"100%\" id=\"linearGradient-1\">\n                                    <stop stop-color=\"#000000\" stop-opacity=\"0\" offset=\"0%\"></stop>\n                                    <stop stop-color=\"#000000\" stop-opacity=\"0.5\" offset=\"100%\"></stop>\n                                </linearGradient>\n                                <filter x=\"-6.2%\" y=\"-17.6%\" width=\"112.5%\" height=\"135.1%\" filterUnits=\"objectBoundingBox\" id=\"filter-2\">\n                                    <feGaussianBlur stdDeviation=\"1.2\" in=\"SourceGraphic\"></feGaussianBlur>\n                                </filter>\n                            </defs>\n                             <polygon class=\"shadow\" fill=\"url(#linearGradient-1)\" filter=\"url(#filter-2)\" transform=\"translate(34.041211, 33.569908) scale(-1, 1) translate(-34.041211, -33.569908) \" points=\"5.18789062 30.0335664 37.2 23.3226562 62.8945312 34.3583711 48.9246094 37.7324922 57.4957031 43.8171602 38.7899414 40.0384492 30.8824219 41.3670625\"></polygon>\n                             <polygon class=\"marker\" fill=\"#78C6C2\" transform=\"translate(14.687695, 22.335937) scale(-1, 1) translate(-14.687695, -22.335937) \" points=\"0 6.14137893 29.3753906 0 29.3753906 30.8630859 18.0421875 33.6676758 18.0421875 44.671875 7.90751953 35.9736328 0 37.2914063\"></polygon>\n                              <g class=\"excavator\" transform=\"translate(25, 11) scale(-.7, .7)\" fill-rule=\"nonzero\" fill=\"#000000\">\n                                    <path d=\"M12.0000143,6.00000715 L6.00000715,6.00000715 L6.00000715,12.0000143 L12.0000143,12.0000143 L12.0000143,6.00000715 Z M14.0000167,16.0000191 L14.0000167,18.0000215 L4.00000477,18.0000215 L4.00000477,16.0000191 L14.0000167,16.0000191 Z M14.0000167,4.00000477 L14.0000167,14.0000167 L4.00000477,14.0000167 L4.00000477,4.00000477 L14.0000167,4.00000477 Z M24.0000286,16.0000191 L24.0000286,18.0000215 L16.0000191,18.0000215 L16.0000191,16.0000191 L24.0000286,16.0000191 Z M24.0000286,12.0000143 L24.0000286,14.0000167 L16.0000191,14.0000167 L16.0000191,12.0000143 L24.0000286,12.0000143 Z M24.0000286,8.00000954 L24.0000286,10.0000119 L16.0000191,10.0000119 L16.0000191,8.00000954 L24.0000286,8.00000954 Z M24.0000286,4.00000477 L24.0000286,6.00000715 L16.0000191,6.00000715 L16.0000191,4.00000477 L24.0000286,4.00000477 Z M26.000031,19.0000226 L26.000031,2.00000238 L2.00000238,2.00000238 L2.00000238,19.0000226 C2.00000238,19.3437731 1.93750231,19.6875235 1.82812718,20.0000238 L25.0000298,20.0000238 C25.5469055,20.0000238 26.000031,19.5468983 26.000031,19.0000226 Z M28.0000334,0 L28.0000334,19.0000226 C28.0000334,20.6562746 26.6562818,22.0000262 25.0000298,22.0000262 L6.34677063e-12,22.0000262 C6.3468722e-12,19.1413023 5.29235914e-12,12.4746277 3.18323146e-12,2.00000238 L3.18323146e-12,0 L28.0000334,0 Z\" fill=\"#000000\"></path>\n                              </g>\n                        </svg>\n                   \n                     "
        });
    };

    IconGenerator.prototype.projectIcon = function projectIcon(item) {

        return L.divIcon({

            iconSize: (45, 63),
            className: 'project ' + item.properties.slug,
            iconAnchor: (0, 0),
            popupAnchor: (0, 0),
            html: "\n                       \n                       \n                            <svg width=\"63px\" height=\"45px\" viewBox=\"0 0 63 45\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n                                <defs>\n                                    <linearGradient x1=\"51.724294%\" y1=\"0%\" x2=\"53.5242129%\" y2=\"100%\" id=\"linearGradient-1\">\n                                        <stop stop-color=\"#000000\" stop-opacity=\"0\" offset=\"0%\"></stop>\n                                        <stop stop-color=\"#000000\" stop-opacity=\"0.5\" offset=\"100%\"></stop>\n                                    </linearGradient>\n                                    <polygon id=\"path-2\" points=\"18.5575702 11.1869203 18.5575702 0.0403432594 0.0222639772 0.0403432594 0.0222639772 11.1869203\"></polygon>\n                                </defs>\n                                <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n                                    <g id=\"projectIcon\" transform=\"translate(0.000000, -11.000000)\">\n                                        <polygon class=\"shadow\"  fill=\"url(#linearGradient-1)\" fill-rule=\"nonzero\" transform=\"translate(34.041211, 44.569908) scale(-1, 1) translate(-34.041211, -44.569908) \" points=\"5.1878908 41.0335664 37.2000002 34.3226562 62.8945314 45.3583711 48.9246096 48.7324922 57.4957033 54.8171602 38.7899416 51.0384492 30.8824221 52.3670625\"></polygon>\n                                        <polygon class=\"marker\" fill=\"#78C6C2\" fill-rule=\"nonzero\" transform=\"translate(14.687695, 33.335938) scale(-1, 1) translate(-14.687695, -33.335938) \" points=\"-6e-07 17.1413789 29.37539 11 29.37539 41.8630859 18.0421869 44.6676758 18.0421869 55.671875 7.90751893 46.9736328 -6e-07 48.2914063\"></polygon>\n                                        <g id=\"Group\" transform=\"translate(3.400000, 24.000000)\">\n                                            <g id=\"Group-3\" transform=\"translate(2.685047, 1.525853)\">\n                                                <g id=\"Fill-1-Clipped\">\n                                                    <mask id=\"mask-3\" fill=\"white\">\n                                                        <use xlink:href=\"#path-2\"></use>\n                                                    </mask>\n                                                    <g id=\"path-1\"></g>\n                                                    <path d=\"M5.70283856,8.65433574 C5.70146864,7.8963936 5.10862107,7.2285561 4.29291454,7.22750388 C3.49479781,7.22656242 2.88463442,7.87147224 2.88008627,8.65234206 C2.87542854,9.44384496 3.5178125,10.0878134 4.3112167,10.0788416 C5.09596299,10.0699807 5.69982473,9.4224126 5.70283856,8.65433574 L5.70283856,8.65433574 Z M12.0008645,8.65477884 C12.0047003,9.4518198 12.6395771,10.0896409 13.4331457,10.0791739 C14.1959184,10.0690393 14.8224661,9.44550642 14.8213702,8.65206516 C14.820329,7.8597315 14.1948773,7.22063664 13.399336,7.22739312 C12.5822596,7.23431574 11.9967548,7.91317404 12.0008645,8.65477884 L12.0008645,8.65477884 Z M12.2711776,0.979493178 C12.1349525,1.66676937 11.9986178,2.35000276 11.8642011,3.03362382 C11.7300583,3.71591573 11.5904906,4.39715542 11.4601288,5.08398856 L17.2874495,5.08398856 C17.2874495,4.93113745 17.284874,4.78559661 17.2887646,4.64016653 C17.2902441,4.58351193 17.271668,4.54280702 17.2358856,4.50104987 C16.8460605,4.04676086 16.4578793,3.59103194 16.0693694,3.1356907 C15.4764122,2.44077196 14.8830715,1.74618549 14.2916486,1.04993761 C14.2451808,0.995165958 14.1972883,0.972792096 14.1247373,0.973179762 C13.5434518,0.97611495 12.9621664,0.974564286 12.380881,0.974785806 C12.3470713,0.974785806 12.3133712,0.977610234 12.2711776,0.979493178 L12.2711776,0.979493178 Z M10.3439709,6.05132562 C10.7232202,4.04504405 11.1007708,2.04767877 11.4799653,0.0414525568 C11.5653388,0.0414525568 11.6437532,0.0413971759 11.7221675,0.0414525568 C12.6510296,0.0416186993 13.5800014,0.042726316 14.5088635,0.04034494 C14.584538,0.0401234167 14.6347319,0.0617773242 14.6848711,0.119539537 C15.4673707,1.02130571 16.2521718,1.92113356 17.0363701,2.82145983 C17.4046052,3.24418176 17.7716348,3.66801131 18.142281,4.08857339 C18.1939544,4.14716632 18.217024,4.20409781 18.2168596,4.28268322 C18.2146129,5.35009348 18.2153252,6.41755914 18.2152705,7.48508016 L18.2152705,7.67924538 L18.5530385,7.67924538 C18.561258,7.96080156 18.5557783,8.23299834 18.5562715,8.51604984 L15.9236096,8.51604984 C16.0061338,9.92216928 14.8903046,11.1509039 13.486463,11.1854062 C12.7579384,11.2032942 12.1306784,10.9489854 11.6209029,10.4238643 C11.1111274,9.89879856 10.8805421,9.25782072 10.9052555,8.51854194 L6.79987236,8.51854194 C6.81943482,9.35518026 6.52923048,10.0538649 5.89999778,10.5938281 C5.40167485,11.0215343 4.81633445,11.2174163 4.16414184,11.1830802 C3.47666004,11.1469165 2.89570339,10.8660249 2.43283405,10.3503739 C1.96958112,9.83433528 1.76442156,9.2195526 1.78376486,8.51532984 L0.0222639772,8.51532984 L0.0222639772,7.68666642 L0.354442735,7.68666642 C0.359922425,7.14116514 0.354168751,6.6043587 0.357730549,6.05271018 C1.0251019,6.04933194 1.68732233,6.05176872 2.34954277,6.051381 C3.01696891,6.05099334 3.68444985,6.05077182 4.351876,6.05077182 C5.01590473,6.05077182 5.67993346,6.05121486 6.34396218,6.05132562 C7.01144316,6.05143638 7.6788693,6.05132562 8.34629544,6.05132562 L10.3439709,6.05132562 Z\" id=\"Fill-1\" fill=\"#151616\" fill-rule=\"nonzero\" mask=\"url(#mask-3)\"></path>\n                                                </g>\n                                            </g>\n                                            <g id=\"Group-6\" transform=\"translate(0.000000, 0.030570)\" fill-rule=\"nonzero\" fill=\"#151616\">\n                                                <path d=\"M4.18350706,3.36649588 C4.16093075,3.5779953 4.17106817,6.15337038 4.19375408,6.2260854 L4.81317815,6.2260854 L4.81317815,3.36649588 L4.18350706,3.36649588 Z M6.6664638,6.22614078 C6.68969772,6.11338542 6.68542356,3.18473597 6.66306642,3.11058103 C6.48645606,3.09308069 6.10994658,3.09989253 6.04051896,3.12176796 C6.01898376,3.39761991 6.02977872,6.14639238 6.05323182,6.22614078 L6.6664638,6.22614078 Z M7.89977748,6.22575312 L8.53580502,6.22575312 L8.53580502,2.85189714 L7.89977748,2.85189714 L7.89977748,6.22575312 Z M9.75974838,2.59487467 L9.75974838,6.22519932 L10.3875563,6.22519932 C10.4106806,6.1038045 10.4050366,2.67357084 10.3816383,2.59487467 L9.75974838,2.59487467 Z M-1.64390675e-05,4.09802136 C0.212650298,3.76202582 0.420220924,3.43433741 0.627517566,3.10642747 C0.842266584,2.76666603 1.05745398,2.42712611 1.27061389,2.08642321 C1.30590309,2.02993475 1.34426091,2.00169052 1.41604484,1.9936603 C1.96921946,1.931523 2.52151733,1.86118934 3.07430838,1.7953969 C3.56906951,1.73641631 4.06426903,1.68086933 4.55908496,1.62216565 C4.99581619,1.5702738 5.43227343,1.51522525 5.86900466,1.46311188 C6.1931283,1.42445606 6.51769026,1.38923384 6.84181386,1.35068878 C7.21711782,1.30599644 7.59220254,1.25864583 7.96756122,1.21373197 C8.23025754,1.18227565 8.4932826,1.15358838 8.7560337,1.12229821 C9.0187848,1.09095265 9.28115232,1.05705958 9.54384864,1.02571403 C9.80659974,0.994479234 10.0696796,0.965625816 10.3324307,0.934612548 C10.5951818,0.90348852 10.8577137,0.870592302 11.1204648,0.839246748 C11.325186,0.814879176 11.5299071,0.79056699 11.7349024,0.768359274 C11.8335367,0.757670772 11.8343039,0.759996768 11.8620311,0.666070872 C11.9161705,0.483147963 11.9695974,0.299948153 12.0236819,0.116969867 C12.0323947,0.0873411186 12.0432992,0.0583215598 12.0545874,0.0248161532 L15.4000472,0.0248161532 C15.4069517,0.325589481 15.4019104,0.622320006 15.4027323,0.929129844 L13.6735068,0.929129844 C13.2557353,2.9930629 12.8394981,5.04946415 12.421617,7.11406176 L12.1788119,7.11406176 C9.10963806,7.11384024 6.04046412,7.11323106 2.97129022,7.11550164 C2.8940814,7.11555702 2.84421623,7.08991572 2.7935839,7.03675014 C1.88028411,6.07688946 0.965669184,5.11835791 0.0515474362,4.15932795 C0.0351083686,4.14210451 0.020751583,4.12288736 -1.64390675e-05,4.09802136 L-1.64390675e-05,4.09802136 Z\" id=\"Fill-4\"></path>\n                                            </g>\n                                        </g>\n                                        <rect id=\"Rectangle\" x=\"0\" y=\"1.77635684e-15\" width=\"62\" height=\"62\"></rect>\n                                    </g>\n                                </g>\n                            </svg>\n                    \n                     "
        });
    };

    return IconGenerator;
}();

var InteractionFilters = function () {
    function InteractionFilters(map, config, contentLayer, interactionPopup) {
        _classCallCheck(this, InteractionFilters);

        this._map = map;
        this._config = config;
        this._contentLayer = contentLayer;
        this._interactionPopup = interactionPopup;
        this.filters = document.getElementById("filter-container");
        this.filterArrayForJs = ['blog', 'project', 'poi'];
        if (this.filters) {
            this.blogFilter = this.filters.querySelector("#blog");
            this.projectFilter = this.filters.querySelector("#project");
            this.poiFilter = this.filters.querySelector("#poi");
        }
    }

    InteractionFilters.prototype.init = function init() {

        var self = this;

        if (self.filters) {
            self.blogFilter.addEventListener("click", function (e) {
                self.filterLayer(self._map, 'icons', 'blog');
            }, false);
            self.projectFilter.addEventListener("click", function (e) {
                self.filterLayer(self._map, 'icons', 'project');
            }, false);

            if (self.poiFilter) {
                self.poiFilter.addEventListener("click", function (e) {
                    self.filterLayer(self._map, 'poi');
                }, false);
            }
        }
    };

    InteractionFilters.prototype.filterLayer = function filterLayer(map, layer, filter) {

        var self = this;

        if (webgl_detect()) {
            self._GL_filterLayer(map, layer, filter);
        } else {
            self._JS_filterLayer(map, layer, filter);
        }
    };

    InteractionFilters.prototype._GL_filterLayer = function _GL_filterLayer(map, layer, filter) {

        var self = this;

        var currentFilter = map.getFilter(layer),
            newFilter = ['any'],
            exists = void 0;

        if (layer === 'icons' || layer === 'icons-active') {

            // check if filter is in array
            exists = currentFilter.filter(function (i) {
                return i[2] === filter;
            });

            if (exists.length > 0) {
                // remove
                currentFilter.forEach(function (f) {
                    if (Array.isArray(f)) {
                        if (f[2] !== filter) {
                            newFilter.push(f);
                        }
                    }
                });

                self.filters.querySelector("#" + filter).classList.add('inactive');
            } else {
                // add
                newFilter = currentFilter;
                newFilter.push(['==', 'type', filter]);

                self.filters.querySelector("#" + filter).classList.remove('inactive');
            }
            self._map.setFilter(layer, newFilter);

            self._map.setFilter('icons-active', ['==', 'type', '']);
            self._interactionPopup.emptyArray();
        }

        if (layer === 'poi') {

            if (self._map.getFilter(layer)[0] === 'has') {
                self._map.setFilter(layer, ['!has', 'name']);
                self.filters.querySelector("#poi").classList.add('inactive');
            } else {
                self._map.setFilter(layer, ['has', 'name']);
                self.filters.querySelector("#poi").classList.remove('inactive');
            }
        }
    };

    InteractionFilters.prototype._JS_filterLayer = function _JS_filterLayer(map, layer, filter) {

        if (layer === 'poi') {
            filter = 'poi';
        }

        var self = this,
            filteredContent = void 0;

        //remove popups
        self._map.closePopup();

        // remove all icons
        self._map.eachLayer(function (l) {
            if (l._icon) {
                l.remove();
            }
        });

        // adapt array with filters
        var inOptionArray = false;

        var index;
        self.filterArrayForJs.forEach(function (filterOption) {
            if (filterOption === filter) {
                inOptionArray = true;
            }
        });

        // adapt array
        if (inOptionArray) {
            index = self.filterArrayForJs.indexOf(filter);
            self.filterArrayForJs.splice(index, 1);
            self.filters.querySelector("#" + filter).classList.add('inactive');
        } else {
            self.filterArrayForJs.push(filter);
            console.log(filter);
            self.filters.querySelector("#" + filter).classList.remove('inactive');
        }

        // check if poi is still in array
        var hasPoi = false;
        self.filterArrayForJs.forEach(function (filterOption) {
            if (filterOption === 'poi') {
                hasPoi = true;
            }
        });

        if (hasPoi) {
            self._poi = new PoiLayer(self._map, self._config);
            self._poi.draw();
        }

        // new features for icon layer
        filteredContent = self._config.dataset.content.features.filter(function (feature) {
            var inFiltersArray = false;
            self.filterArrayForJs.forEach(function (filterOption) {
                if (feature.properties.type === filterOption) {
                    inFiltersArray = true;
                }
            });

            if (inFiltersArray) {
                return feature;
            };
        });

        // if features redraw icon layer
        if (filteredContent.length > 0) {

            var filteredFeatureCollection = {
                type: "FeatureCollection",
                features: filteredContent
            };
        }

        // this._contentLayer = new ContentLayer(self._map, self._config);
        this._contentLayer.draw(filteredFeatureCollection);
    };

    return InteractionFilters;
}();

var InteractionPage = function () {
    function InteractionPage(map, config, popup) {
        _classCallCheck(this, InteractionPage);

        this._map = map;
        this._config = config;
        this.interactionPopup = popup;

        this._hostContainer = document.getElementById('kaart');
        this._explanationContainer = document.getElementById('kaart-explanation');

        this._allLocations = document.getElementById('all_locations');
        this._relevantLocations = document.getElementById('relevant_locations');
        this._selectedLocations = document.getElementById('selected_locations');

        this._tabAlles = document.getElementById('tab_alles');
        this._tabActueel = document.getElementById('tab_actueel');
        this._tabSelection = document.getElementById('tab_selectie');
    }

    InteractionPage.prototype.init = function init() {

        var self = this;

        var hideButtons = [].slice.call(document.getElementsByClassName('hide-explanation'));

        if (hideButtons) {
            hideButtons.forEach(function (b) {
                b.addEventListener("click", function () {
                    self.hideExplanation();
                }, false);
            });
        }

        var showButtons = [].slice.call(document.getElementsByClassName('show-explanation'));

        if (showButtons) {
            showButtons.forEach(function (b) {
                b.addEventListener("click", function () {
                    self.showExplanation();
                }, false);
            });
        }

        self._map.on('click', 'labels', function (e) {
            self._showLocationInfo(e.features[0].properties.slug);
        });

        this._tabAlles.addEventListener("click", function () {
            self._switchInfo('alles');
        }, false);

        this._tabActueel.addEventListener("click", function () {
            self._switchInfo('actueel');
        }, false);

        this._tabSelection.addEventListener("click", function () {
            self._switchInfo('selectie');
        }, false);
    };

    InteractionPage.prototype._showLocationInfo = function _showLocationInfo(slug) {

        var self = this;
        this._selectedLocations.innerHTML = '';
        self._switchInfo('selectie');
        var locationElement = [].slice.call(document.querySelectorAll('.location.' + slug))[0];
        var div = document.createElement("div");
        div.classList.add("location");
        div.classList.add(slug);
        div.innerHTML = locationElement.innerHTML;
        this._selectedLocations.appendChild(div);
    };

    InteractionPage.prototype.showExplanation = function showExplanation() {

        this._explanationContainer.style.display = 'block';
        localStorage.setItem('map-explanation', true);
    };

    InteractionPage.prototype.hideExplanation = function hideExplanation() {

        this._explanationContainer.style.display = 'none';
        localStorage.setItem('map-explanation', false);
    };

    InteractionPage.prototype._switchInfo = function _switchInfo(keuze) {

        this._keuze = keuze;

        if (this._keuze === 'alles') {

            this._allLocations.style.display = 'block';
            this._relevantLocations.style.display = 'none';
            this._selectedLocations.style.display = 'none';

            this._tabAlles.classList.add('active');
            this._tabActueel.classList.remove('active');
            this._tabSelection.classList.remove('active');
        } else if (this._keuze === 'actueel') {

            this._allLocations.style.display = 'none';
            this._selectedLocations.style.display = 'none';
            this._relevantLocations.style.display = 'block';

            this._tabAlles.classList.remove('active');
            this._tabActueel.classList.add('active');
            this._tabSelection.classList.remove('active');
        } else if (this._keuze === 'selectie') {

            this._allLocations.style.display = 'none';
            this._selectedLocations.style.display = 'block';
            this._relevantLocations.style.display = 'none';

            this._tabAlles.classList.remove('active');
            this._tabActueel.classList.remove('active');
            this._tabSelection.classList.add('active');
        }
    };

    return InteractionPage;
}();

var Interaction3D = function () {
    function Interaction3D(map, config) {
        _classCallCheck(this, Interaction3D);

        this._map = map;
        this._config = config;
        this.button_2d = null;
        this.button_3d = null;
        // this.button_three_d = document.getElementById("three-d-button");
        // this.button_two_d = document.getElementById("two-d-button");
        // this.dimensionSelector = document.getElementById("dimension-selector");
    }

    Interaction3D.prototype.init = function init(open) {

        var self = this,
            buildings = {},
            xhr = new XMLHttpRequest();
        // enable dimension toggle buttons
        self._createButtons();

        xhr.open('GET', 'https://api.mapbox.com/datasets/v1/wijnemenjemee/cj056h8w200c033o0mcnvk5nt/features?access_token=pk.eyJ1Ijoid2lqbmVtZW5qZW1lZSIsImEiOiJjaWgwZjB4ZGwwMGdza3FseW02MWNxcmttIn0.l-4VI25pfA5GKukRQTXnWA');
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4;var OK = 200;
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {

                    var threeDStructures = JSON.parse(xhr.response);
                    buildings.type = "FeatureCollection";
                    buildings.features = threeDStructures.features;
                    // self.mergeContent(customfeatures);
                    self._drawEssentials(buildings);

                    if (open) {
                        self.shiftPitch(self._map, 75);
                    }
                } else {
                    console.log('kan features niet ophalen bij mapboxxx'); // dan weer eruit halen // An error occurred during the request.
                }
            }
        };
    };

    Interaction3D.prototype.shiftPitch = function shiftPitch(map, pitch) {

        var self = this;

        self.button_3d.classList.remove('visible');
        self.button_2d.classList.add('visible');

        var center = map.getCenter();
        var zoom = map.getZoom() + .5;
        var bearing = map.getBearing();

        self._show();

        map.flyTo({
            center: center,
            zoom: zoom,
            pitch: pitch || 75,
            bearing: bearing,
            speed: 0.2, // make the flying slow
            curve: .75, // change the speed at which it zooms out
            easing: function easing(t) {
                return t;
            }
        });
    };

    Interaction3D.prototype.resetPitch = function resetPitch(map) {

        var self = this;

        self.button_2d.classList.remove('visible');
        self.button_3d.classList.add('visible');

        var center = map.getCenter();
        var zoom = map.getZoom() - .5;
        var bearing = map.getBearing();

        self._hide(2000);

        map.flyTo({
            center: center,
            zoom: zoom,
            pitch: 0,
            bearing: bearing,
            speed: 0.2, // make the flying slow
            curve: .75, // change the speed at which it zooms out
            easing: function easing(t) {
                return t;
            }
        });
    };

    Interaction3D.prototype._createButtons = function _createButtons() {

        var self = this;

        self.dimensionSelector = document.createElement('div');
        self.dimensionSelector.id = 'dimension-selector';

        self.button_2d = document.createElement('div');
        self.button_2d.id = 'two-d-button';
        self.button_2d.innerHTML = '2D';
        self.dimensionSelector.appendChild(self.button_2d);

        self.button_3d = document.createElement('div');
        self.button_3d.id = 'three-d-button';
        self.button_3d.innerHTML = '3D';
        self.dimensionSelector.appendChild(self.button_3d);

        self.button_3d.classList.add('visible');
        self.button_3d.addEventListener("click", function () {
            self.shiftPitch(self._map);
        }, false);
        self.button_2d.addEventListener("click", function () {
            self.resetPitch(self._map);
        }, false);

        document.getElementById(self._config.hostContainer).appendChild(self.dimensionSelector);
    };

    Interaction3D.prototype._drawEssentials = function _drawEssentials(data) {

        var self = this;

        // if (self._map.getSource("buildings") === undefined) {

        self._map.addSource("buildings", {
            "type": "geojson",
            "data": data
        });
        // }

        self._map.addLayer({
            "id": "buildings",
            "source": "buildings",
            "filter": ["all", ["!has", 'class'], ["has", 'base'], ["has", 'height']], //  ,["has",'colour']
            "type": "fill-extrusion",
            "paint": {

                "fill-extrusion-base": {
                    "type": "identity",
                    "property": "base"
                },
                "fill-extrusion-color": "rgb(216,216,216)",
                // "fill-extrusion-color": {
                //     "type": "identity",
                //     "property": "colour"  // colour
                // },
                "fill-extrusion-height": {
                    "type": "identity",
                    "property": "height"
                },
                "fill-extrusion-opacity": .5
            }

        }, 'poi');

        self._hide(0);
    };

    Interaction3D.prototype._show = function _show() {
        var self = this;
        setTimeout(function () {
            self._map.setPaintProperty('buildings', 'fill-extrusion-opacity', 1);
        }, 0);
    };

    Interaction3D.prototype._hide = function _hide(time) {

        var self = this;
        setTimeout(function () {
            self._map.setPaintProperty('buildings', 'fill-extrusion-opacity', 0);
        }, time);
    };

    return Interaction3D;
}();

var InteractionPopup = function () {
    function InteractionPopup() {
        _classCallCheck(this, InteractionPopup);

        // this._map = map;
        // this._config = config;
        this.popup = null;
        this.popups = [];
    }

    InteractionPopup.prototype.createPopup = function createPopup(item) {

        var url = void 0;

        if (item.properties.slug === 'zuidasdok') {
            url = 'zuidasdok';
        } else {
            url = item.properties.url;
        }

        if (item.properties.third_line) {
            return "<a href=\"" + url + "\">\n                        <span class=\"multiline\">\n                            " + item.properties.first_line + "\n                        </span><br/>\n                        <span class=\"multiline\">\n                            " + item.properties.second_line + "\n                        </span><br/>\n                        <span class=\"multiline\">\n                            " + item.properties.third_line + "\n                        </span>\n                    </a>\n                         ";
        } else if (item.properties.second_line) {
            return "<a href=\"" + url + "\">\n                        <span class=\"multiline\">\n                            " + item.properties.first_line + "\n                        </span><br/>\n                        <span class=\"multiline\">\n                            " + item.properties.second_line + "\n                        </span>\n                     </a>\n                         ";
        } else {

            return "<a href=\"" + url + "\">\n                        <span class=\"multiline\">\n                            " + item.properties.title + "\n                        </span>\n                   </a>";
        }
    };

    InteractionPopup.prototype.openPopup = function openPopup(map, html, lngLat, type, offset, closeOthers) {

        var self = this;
        if (closeOthers && self.popup) {
            self.popup.remove();
        }
        self.popup = new mapboxgl.Popup({ offset: offset, anchor: 'top-left', closeButton: false }).setLngLat(lngLat).setHTML(html).addTo(map);
        self.popup._container.classList.add(type);

        self.popups.push(self.popup);
    };

    InteractionPopup.prototype.closePopup = function closePopup() {

        var self = this;
        if (self.popup) {
            self.popup.remove();
        }
    };

    // closeAllPopups(map) {
    //
    //
    //     // loop door lagen
    //     // verzamel iconen
    //     // verzamel open popups
    //
    //     let popup,
    //         activeIcons = map.queryRenderedFeatures({ layers: ['icons-active'] });
    //
    //     activeIcons.forEach( function(icon) {
    //
    //      //   popup = icon.getPopup();
    //         console.log(icon);
    //     })
    //
    // }


    InteractionPopup.prototype.addToArray = function addToArray(popup) {

        var self = this;
        self.popups.push(self.popup);
    };

    InteractionPopup.prototype.emptyArray = function emptyArray(map) {

        var self = this;

        if (webgl_detect() && !isIE11()) {

            self.popups.forEach(function (popup) {

                popup.remove();
            });
        } else {

            map.closePopup();
        }
    };

    return InteractionPopup;
}();

var Lines = function () {
    function Lines(map, config) {
        _classCallCheck(this, Lines);

        this._map = map;
        this._config = config;

        this.speedFactor = 30; // number of frames per longitude degree
        this.animation = ''; // to store and cancel the animation
        this.startTime = 0;
        this.progress = 0; // progress = timestamp - startTime
        this.resetTime = false; // indicator of whether time reset is needed for the animation
        this.layerId = 'cjc4zc40d13wa2wqskv9bk020';
    }

    Lines.prototype.init = function init() {

        var self = this;
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://api.mapbox.com/datasets/v1/wijnemenjemee/' + self.layerId + '/features?access_token=' + self._config.accessToken);
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {

                    var data = JSON.parse(xhr.response);

                    data.features = data.features.filter(function (feature) {
                        console.log(feature.geometry.type);
                        return feature.geometry.type == 'LineString';
                    });
                    self._draw(data);
                }
            }
        };
    };

    Lines.prototype._draw = function _draw(lines) {

        var self = this;

        // self.lines = lines;

        self._map.addSource("lines", {
            "type": "geojson",
            "data": lines
        });

        self._map.addLayer({
            "id": "Amstelveenlijn",
            "type": "line",
            "source": "lines",
            "layout": {
                "line-join": "miter",
                "line-cap": "square"
            },
            "paint": {
                "line-color": donkergeel,
                "line-width": 8,
                "line-dasharray": [.5, .5]
            }
        }, 'stops');

        // let startTime = performance.now();
        //
        // self._animateLine();
    };

    Lines.prototype.zoom = function zoom() {

        var self = this,
            coordinates = self._config.dataset.lines.features[1].geometry.coordinates,
            bounds = coordinates.reduce(function (bounds, coord) {
                return bounds.extend(coord);
            }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

        self._map.fitBounds(bounds, {
            padding: 100
        });
    };

    Lines.prototype.addLegend = function addLegend() {

        var legend = document.createElement('div');
        legend.classList.add('legend');
        self.hostContainer.appendChild(legend);
    };

    Lines.prototype._animateLine = function _animateLine(timestamp) {

        var self = this;

        if (self.resetTime) {
            // resume previous progress
            self.startTime = performance.now() - self.progress;
            self.resetTime = false;
        } else {
            self.progress = timestamp - self.startTime;
        }

        // restart if it finishes a loop
        if (self.progress > self.speedFactor * 360) {
            self.startTime = timestamp;
            self.lines.features[0].geometry.coordinates = [];
        } else {
            var x = self.progress / self.speedFactor;
            // draw a sine wave with some math.
            var y = Math.sin(x * Math.PI / 90) * 40;
            // append new coordinates to the lineString
            self.lines.features[0].geometry.coordinates.push([x, y]);
            // then update the map
            self._map.getSource('lines').setData(self.lines);
        }
        // Request the next frame of the animation.
        self.animation = requestAnimationFrame(self._animateLine);
    };

    return Lines;
}();

var ListReference = function () {
    function ListReference(popup) {
        _classCallCheck(this, ListReference);

        this.interactionPopup = popup;
    }

    ListReference.prototype.generateHTML = function generateHTML(features) {

        var self = this,
            ul = void 0,
            li = void 0;

        ul = document.createElement('ul');

        self.features = features;

        self.features.forEach(function (feature) {
            li = document.createElement('li');
            if (feature.properties.slug === 'zuidasdok') {
                li.innerHTML = "\n                        <span class=\"evensmallerfont\" data-slug=\"zuidasdok\">" + feature.properties.title + "</span>\n                ";
            } else {
                li.innerHTML = " \n                        <span class=\"evensmallerfont\" data-slug=\"" + feature.properties.slug + "\">" + feature.properties.title + "</span>\n                ";
            }
            ul.appendChild(li);
        });
        return ul;
    };

    ListReference.prototype.generateTabs = function generateTabs() {

        var self = this,
            ul = void 0,
            tab_map = void 0,
            tab_list = void 0;

        ul = document.createElement('ul');

        tab_map = document.createElement('li');
        tab_map.innerHTML = 'Kaart';
        tab_map.id = "tab_map";
        tab_map.classList.add('active');
        ul.appendChild(tab_map);

        tab_list = document.createElement('li');
        tab_list.innerHTML = 'Lijst';
        tab_list.id = "tab_list";
        ul.appendChild(tab_list);

        return ul;
    };

    ListReference.prototype.addEventListeners = function addEventListeners(map, container) {

        var self = this,
            slug = void 0,
            linkList = [].slice.call(container.querySelectorAll('ul>li>span'));

        linkList.forEach(function (link) {
            link.addEventListener('click', function (event) {

                if (event && event.target.attributes['data-slug']) {

                    self.focus(map, event.target.attributes['data-slug'].nodeValue);
                }
            }, false);
        });

        // self.interactionPopup = new InteractionPopup(map,'');
    };

    ListReference.prototype.addEventListenersMobile = function addEventListenersMobile() {

        var projects_map = void 0,
            map_list_container = void 0;

        projects_map = document.getElementById('projects-map');
        map_list_container = document.getElementById('map-list-container');

        setTimeout(function () {
            var tm = document.getElementById('tab_map');
            var tl = document.getElementById('tab_list');
            if (tm) {
                tm.addEventListener('click', function (event) {
                    if (projects_map) {
                        projects_map.style.visibility = 'visible';projects_map.style.height = '100%';
                    }
                    if (map_list_container) {
                        map_list_container.style.visibility = 'hidden';
                    }
                    tm.classList.add('active');
                    tl.classList.remove('active');
                });
            }

            if (tl) {
                tl.addEventListener('click', function (event) {
                    if (projects_map) {
                        projects_map.style.visibility = 'hidden';projects_map.style.height = '0px';
                    }
                    if (map_list_container) {
                        map_list_container.style.visibility = 'visible';
                    }
                    tm.classList.remove('active');
                    tl.classList.add('active');
                });
            }
        }, 500);
    };

    ListReference.prototype.focus = function focus(map, slug) {
        if (webgl_detect() && !isIE11()) {
            this._GL_Focus(map, slug);
        } else {
            this._JS_Focus(map, slug);
        }
    };

    // focusOff(map,slug) {
    //     if (webgl_detect() && !isIE11()) {
    //         this._GL_UnFocus(map,slug);
    //     } else {
    //         this._JS_UnFocus(map,slug);
    //     }
    // }

    ListReference.prototype._GL_Focus = function _GL_Focus(map, slug) {

        var self = this,
            html = void 0,
            offset = void 0,
            lngLat = void 0,
            type = void 0,
            features = map.queryRenderedFeatures({ filter: ['==', 'slug', slug], layers: ['icons'] });

        if (features[0]) {

            if (window.innerWidth <= 760) {
                var projects_map = document.getElementById('projects-map');
                if (projects_map) {
                    projects_map.style.visibility = 'visible';projects_map.style.height = '100%';
                }
                var map_list_container = document.getElementById('map-list-container');
                if (map_list_container) {
                    map_list_container.style.visibility = 'hidden';
                }
                var tm = document.getElementById('tab_map');
                var tl = document.getElementById('tab_list');
                tm.classList.add('active');
                tl.classList.remove('active');
            }

            html = self.interactionPopup.createPopup(features[0]);
            offset = [-2, -14];
            lngLat = features[0].geometry.coordinates;
            type = features[0].properties.type;

            self.interactionPopup.openPopup(map, html, lngLat, type, offset, true);

            map.easeTo({

                center: [lngLat[0] + 0.001, lngLat[1] - 0.0005],
                zoom: 14,
                speed: 0.2,
                curve: 1
            });
        }
    };

    ListReference.prototype._JS_Focus = function _JS_Focus(map, slug) {

        var self = this,
            html = void 0,
            latLng = void 0,
            highlightedMarker = void 0,
            highlightedFeature = void 0;

        // all features
        // filter by slug ?
        highlightedFeature = self.features.filter(function (f) {
            return f.properties.slug === slug;
        });

        // highlightedMarker = document.querySelector('.marker.' + slug);
        // console.log(highlightedMarker);
        // highlightedMarker.style.backgroundImage = 'url("/assets/svg/highlight-icon-construction-project.svg")';

        html = self.interactionPopup.createPopup(highlightedFeature[0]);
        latLng = new L.LatLng(highlightedFeature[0].geometry.coordinates[1], highlightedFeature[0].geometry.coordinates[0]);
        console.log(latLng);
        //open popup;
        var popup = L.popup({
            className: highlightedFeature[0].properties.type,
            offset: [0, 36]
        }).setLatLng(latLng).setContent(html).openOn(map);
    };

    // _GL_UnFocus(map,slug) {
    //
    //     let self = this;
    //
    //     self.interactionPopup.closePopup();
    // }
    //
    // _JS_UnFocus(map,slug) {
    //
    //     let allMarkers = document.querySelectorAll('.marker');
    //
    //     [].forEach.call(allMarkers, function(marker) {
    //         marker.style.backgroundImage = 'url("/assets/svg/icon-construction-project.svg")';
    //     });
    // }


    return ListReference;
}();

var MapJs = function () {
    function MapJs(config) {
        _classCallCheck(this, MapJs);

        this._config = config;
    }

    MapJs.prototype.create = function create() {

        var self = this;
        console.log(self._config);
        L.mapbox.accessToken = self._config.accessToken;
        this._map = L.mapbox.map(self._config.hostContainer).setView([self._config.center[1], self._config.center[0]], self._config.zoom);
        var styleLayer = L.mapbox.styleLayer(self._config.style).addTo(this._map);

        console.log(self._config.scrollzoom);
        console.log(self._map.scrollWheelZoom);
        if (self._map.scrollWheelZoom && self._config.scrollzoom === false) {
            console.log('disable');
            self._map.scrollWheelZoom.disable();
        }

        return this._map;
    };

    return MapJs;
}();

var MapWebGL = function () {
    function MapWebGL(config) {
        _classCallCheck(this, MapWebGL);

        this._config = config;
    }

    MapWebGL.prototype.create = function create() {

        var self = this;

        mapboxgl.accessToken = self._config.accessToken;

        this._map = new mapboxgl.Map({
            attributionControl: false,
            container: self._config.hostContainer,
            center: self._config.center,
            zoom: self._config.zoom,
            minZoom: 13,
            maxZoom: 20,
            style: self._config.style,
            scrollZoom: self._config.scrollZoom,
            pitch: self._config.pitch,
            bearing: self._config.bearing
        });

        var nav = new mapboxgl.NavigationControl();
        self._map.addControl(nav, 'top-left');
        //
        // .addControl(new mapboxgl.AttributionControl({
        //     compact: true
        // }));

        if (self._config.scrollzoom === false) {
            self._map.scrollZoom.disable();
        }

        return this._map;
    };

    MapWebGL.prototype._drawContentFromTemplate = function _drawContentFromTemplate() {

        var self = this;
        self._map.on('load', function () {
            self._addIcons(self.templateContent);
        });
    };

    MapWebGL.prototype._drawPoints = function _drawPoints() {

        var self = this;

        self._map.on('load', function () {

            self._map.addSource("points", {
                "type": "geojson",
                "data": self.dataset.points
            });

            self._map.addLayer({
                "id": "references",
                "type": "circle",
                "source": "points",
                "paint": {
                    "circle-color": {
                        property: 'class',
                        type: 'categorical',
                        stops: [['omleiding', '#4C9630'], ['beperking', '#DC3D50'], ['geluidsmeter', '#4C9630']]
                    },
                    "circle-radius": {
                        property: 'class',
                        type: 'categorical',
                        stops: [['omleiding', 20], ['beperking', 20], ['geluidsmeter', 2]]
                    },
                    "circle-opacity": {
                        property: 'class',
                        type: 'categorical',
                        stops: [['omleiding', 1], ['beperking', 1], ['geluidsmeter', 1]]

                    },
                    "circle-stroke-width": {
                        property: 'class',
                        type: 'categorical',
                        stops: [['omleiding', 0], ['beperking', 0], ['geluidsmeter', 18]]

                    },
                    "circle-stroke-color": {
                        property: 'class',
                        type: 'categorical',
                        stops: [['omleiding', '#4C9630'], ['beperking', '#DC3D50'], ['geluidsmeter', '#4C9630']]
                    },
                    "circle-stroke-opacity": {
                        property: 'class',
                        type: 'categorical',
                        stops: [['omleiding', 1], ['beperking', 1], ['geluidsmeter', .7]]

                    }
                }
            });

            self._map.addLayer({
                "id": "labels",
                "type": "symbol",
                "source": "points",
                'layout': {
                    'visibility': 'visible',
                    'text-field': '{reference}',
                    "text-font": ["Arial Unicode MS Bold"], // "Cabrito Sans W01 Norm Bold",
                    "text-size": 14,
                    "text-offset": [0, -0.1]
                },
                'paint': {
                    "text-color": "#fff"
                }
            });
        });
    };

    MapWebGL.prototype._initPopup = function _initPopup() {

        var self = this;

        self._map.on('style.load', function () {
            // Create a popup, but don't add it to the map yet.
            var popup = new mapboxgl.Popup({
                closeButton: false,
                closeOnClick: false
            });
        });
    };

    return MapWebGL;
}();

var PoiLayer = function () {
    function PoiLayer(map, config) {
        _classCallCheck(this, PoiLayer);

        this._map = map;
    }

    PoiLayer.prototype.draw = function draw() {

        var self = this;

        if (webgl_detect()) {
            var _layout;

            self._map.addSource("poi", {
                "type": "geojson",
                "data": "./assets/geojson/poi.geojson"
            });

            self._map.addLayer({
                "id": "poi",
                "type": "symbol",
                "source": "poi",
                "layout": (_layout = {
                    "visibility": "visible",
                    "icon-image": "rect_white",
                    "icon-padding": 0,
                    "icon-text-fit": 'both',
                    "icon-text-fit-padding": [10, 5, 0, 5],
                    "icon-allow-overlap": true
                }, _defineProperty(_layout, "icon-padding", 20), _defineProperty(_layout, "text-field", "{name}"), _defineProperty(_layout, "text-font", ["BC Falster Grotesk Bold"]), _defineProperty(_layout, "text-size", 20), _defineProperty(_layout, "text-offset", [0, 0]), _defineProperty(_layout, "text-anchor", "center"), _layout),
                'paint': {
                    "text-color": "rgb(51,51,51)"
                },
                'filter': ['has', 'name']
            }, 'projects');
        } else {

            var markers = [];

            var poi = L.mapbox.featureLayer().loadURL('./assets/geojson/poi.geojson').on('ready', function (e) {

                e.target._geojson.features.forEach(function (feature) {

                    var label = L.divIcon({

                        iconSize: (25, 200),
                        className: 'poi label',
                        iconAnchor: (50, 50),
                        popupAnchor: (0, 0),
                        html: "\n                                <span class=\"js-label\">" + feature.properties.name + "</span>\n                            "
                    });

                    var marker = L.marker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], { icon: label }); // .bindPopup(item.title);
                    markers.push(marker);
                });

                this.markers = new L.featureGroup(markers);
                this.markers.addTo(self._map);
            });
        }
    };

    return PoiLayer;
}();

var Points = function () {
    function Points(map, config) {
        _classCallCheck(this, Points);

        this._map = map;
        this._config = config;
    }

    Points.prototype.draw = function draw(points) {

        var self = this;

        self._map.addSource("stops", {
            "type": "geojson",
            "data": points
        });

        self._map.addLayer({
            "id": "stops",
            "type": "circle",
            "source": "stops",
            "paint": {
                "circle-color": "#fff",
                "circle-radius": 6,
                "circle-opacity": 1,
                "circle-stroke-width": 6,
                "circle-stroke-color": {
                    property: 'stopType',
                    type: 'categorical',
                    stops: [['remove', donkerrood], ['inherit', donkergeel], ['merge', navy]]
                },
                "circle-stroke-opacity": 1
            }
        });

        self._map.addLayer({
            "id": "labels",
            "type": "symbol",
            "source": "stops",
            "layout": {
                "text-font": ["Cabrito Semi W01 Norm E ExtraBold"],
                "text-field": "{name}",
                "symbol-placement": "point",
                "text-size": 20,
                "text-anchor": "left",
                "text-offset": [1.5, 0],
                "text-max-width": 30
            },
            "paint": {
                'text-color': '#ffffff'
            }
        });

        self._map.addLayer({
            "id": "crossings",
            "type": "symbol",
            "source": "stops",
            "layout": {
                "visibility": "visible",
                "icon-image": "crossing",
                "icon-padding": 0,
                "icon-text-fit": 'both',
                "icon-allow-overlap": true
            },
            "filter": ["==", "crossing", true]
        }, 'stops');
    };

    Points.prototype.zoom = function zoom() {

        var self = this,
            coordinates = self._config.dataset.lines.features[1].geometry.coordinates,
            bounds = coordinates.reduce(function (bounds, coord) {
                return bounds.extend(coord);
            }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

        self._map.fitBounds(bounds, {
            padding: 100
        });
    };

    Points.prototype.addLegend = function addLegend() {

        var legend = document.createElement('div');
        legend.classList.add('legend');
        self.hostContainer.appendChild(legend);
    };

    return Points;
}();

var Map = function () {
    function Map(element, data, interaction) {
        _classCallCheck(this, Map);

        this.element = document.getElementById(element);

        try {
            var dataConfig = this.element.getAttribute('data-config');
            this.customConfig = JSON.parse(dataConfig);
        } catch (err) {
            this.customConfig = [];
            console.log(err);
        }

        var self = this,
            activeItems = [];

        this.config = {
            accessToken: 'pk.eyJ1Ijoid2lqbmVtZW5qZW1lZSIsImEiOiJjaWgwZjB4ZGwwMGdza3FseW02MWNxcmttIn0.l-4VI25pfA5GKukRQTXnWA',
            style: 'mapbox://styles/wijnemenjemee/cixls3wf0001o2ro0xjvltjaj',
            hostContainer: element,
            center: [4.875, 52.3],
            zoom: 5,
            pitch: 0,
            bearing: 0,
            scrollZoom: false
        };

        this.options = {
            // interaction: interaction,
            filters: true,
            referenceList: true
        };

        //data from argument in footer scripts (datasets)
        if (data) {
            var d = this.config.dataset = JSON.parse(data);
        } else {
            this.config.dataset = {};
        }

        this.interactionPopup = new InteractionPopup();
    }

    Map.prototype.init = function init() {

        var self = this;

        /* WEBGL */

        if (webgl_detect()) {

            var mapWebGL = new MapWebGL(self.config);

            self._map = mapWebGL.create();

            self._lines = new Lines(self._map, self.config);
            self._points = new Points(self._map, this.config.dataset);
            self.interactionPage = new InteractionPage(self._map, self.config); // ,self.interactionPopup,self._activeMarkers
            self.interactionPage.init();

            self._map.on('style.load', function () {

                self._lines.init();

                if (self.config.dataset && self.config.dataset.features.length > 0) {
                    self._points.draw(self.config.dataset);
                }
            });
        } else {

            /* JS */

            var mapJs = new MapJs(self.config);
            self._map = mapJs.create();
            self._donut = new Donut(self._map, self.config);
            self._donut.draw();

            if (this.customConfig && this.customConfig.interaction === 'navigation') {
                self.interactionPage = new InteractionPage(self._map, self.config, self.interactionPopup);
                self.interactionPage.init();
            }

            self._center = new Center(self._map, self.config);
            self._activeMarkers = new ActiveMarkers(self._map, self.config, self.interactionPopup, self._center);

            self._content = new ContentLayer(self._map, self.config, self.interactionPopup, self._activeMarkers);
            if (self.config.dataset.content && self.config.dataset.content.features.length > 0) {
                self._content.draw(self.config.dataset.content);
            }

            if (self.options.filters) {
                self.interactionFilters = new InteractionFilters(self._map, self.config, self._content);
                self.interactionFilters.init();
            }

            self._poi = new PoiLayer(self._map, self.config);
            self._poi.draw();
        }
    };

    return Map;
}();