'use strict';

/**
 * Remove json data from the news queue
 */
module.exports = function() {

    let gulp = require('gulp'),
        babel = require('gulp-babel'),
        plumber = require('gulp-plumber'),
        concat = require('gulp-concat'),
        merge = require('merge-stream'),
        path = require('../local-path')();

    let scriptsDir = path.productionFolder + '/assets/scripts/pages/';

    gulp.task('js-compile', function() {

		var page = gulp.src([
                path.projectFolder + '/scripts/common/constants.js',
                // path.projectFolder + '/scripts/storytelling/main.js',
                path.projectFolder + '/scripts/storytelling/video.js',
              //  path.projectFolder + '/scripts/expand-content/main.js',
                path.projectFolder + '/scripts/menu/menu.js',
                // path.projectFolder + '/scripts/forms/newsletter.js',
                // path.projectFolder + '/scripts/forms/contact.js',
			])
			.pipe(plumber())
			.pipe(concat('page.js'))
			.pipe(babel({
				presets: ['es2015']
			}))
			.pipe(gulp.dest(scriptsDir));

        var theme = gulp.src([
            path.projectFolder + '/scripts/common/constants.js',
            // path.projectFolder + '/scripts/storytelling/main.js',
            // path.projectFolder + '/scripts/storytelling/video.js',
            //  path.projectFolder + '/scripts/expand-content/main.js',
            path.projectFolder + '/scripts/menu/menu.js',
            // path.projectFolder + '/scripts/forms/newsletter.js',
            // path.projectFolder + '/scripts/forms/contact.js',
        ])
            .pipe(plumber())
            .pipe(concat('theme-wijnemenjemee.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));


        var pageWB = gulp.src([
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/common/breakpoints.js',
            path.projectFolder + '/scripts/common/colours.js',
            path.projectFolder + '/scripts/menu/menu-wb.js'
        ])
            .pipe(plumber())
            .pipe(concat('page-werkenbij.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));


        var english = gulp.src([
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/storytelling/main.js',
            path.projectFolder + '/scripts/menu/menu.js',
            path.projectFolder + '/scripts/forms/newsletter.js'
        ])
            .pipe(plumber())
            .pipe(concat('english.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var homepage = gulp.src([
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/common/breakpoints.js',
            path.projectFolder + '/scripts/common/colours.js',
            path.projectFolder + '/scripts/external-sources/twitter.js',
            path.projectFolder + '/scripts/external-sources/youtube.js',
            path.projectFolder + '/scripts/external-sources/instagram.js',
            path.projectFolder + '/scripts/menu/menu.js',
            path.projectFolder + '/scripts/menu/dashboard-menu.js',
            path.projectFolder + '/scripts/menu/slideshow.js',
            path.projectFolder + '/scripts/forms/newsletter.js',
            path.projectFolder + '/scripts/scrolling/scrollmethods-home.js',

            ])
            .pipe(plumber())
            .pipe(concat('homepage-wijnemenjemee.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var homepageWB = gulp.src([
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/common/breakpoints.js',
            path.projectFolder + '/scripts/common/colours.js',
            path.projectFolder + '/scripts/menu/menu-wb.js',
            path.projectFolder + '/scripts/menu/videoloop.js'

        ])
            .pipe(plumber())
            .pipe(concat('homepage-werkenbij.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var post = gulp.src([
                path.projectFolder + '/scripts/common/constants.js',
                path.projectFolder + '/scripts/storytelling/social-dialogue.js',
                path.projectFolder + '/scripts/storytelling/video.js',
                path.projectFolder + '/scripts/storytelling/appreciation.js',
                path.projectFolder + '/scripts/dialoog/commenting.js',
                path.projectFolder + '/scripts/lazyloading/main.js',
                path.projectFolder + '/scripts/menu/menu.js',
                path.projectFolder + '/scripts/forms/newsletter.js'
            ])
            // .pipe(plumber())
            .pipe(concat('post-wijnemenjemee.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));


        var page = gulp.src([
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/storytelling/video.js',
            path.projectFolder + '/scripts/lazyloading/main.js',
            path.projectFolder + '/scripts/menu/menu.js',
        ])
        // .pipe(plumber())
            .pipe(concat('page-wijnemenjemee.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var vacature = gulp.src([
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/storytelling/social-dialogue.js',
            path.projectFolder + '/scripts/storytelling/video.js',
            // path.projectFolder + '/scripts/storytelling/appreciation.js',
            // path.projectFolder + '/scripts/dialoog/commenting.js',
            path.projectFolder + '/scripts/lazyloading/main.js',
            path.projectFolder + '/scripts/menu/menu-wb.js'

        ])
            .pipe(concat('vacature-werkenbij.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var ambassador = gulp.src([
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/storytelling/social-dialogue.js',
            path.projectFolder + '/scripts/storytelling/video.js',
            path.projectFolder + '/scripts/menu/portretvideos.js',
            path.projectFolder + '/scripts/lazyloading/main.js',
            path.projectFolder + '/scripts/menu/menu-wb.js'
        ])
        // .pipe(plumber())
            .pipe(concat('ambassador-werkenbij.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));


        var search = gulp.src([
                path.projectFolder + '/scripts/menu/menu.js',
            ])
            .pipe(plumber())
            .pipe(concat('search.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));
        


        var searchIndex = gulp.src([
            './scripts/search/search-url-parameters.js',
            './scripts/search/search-checkboxes.js',
            './scripts/search/search-dropdown.js',
            './scripts/search/search-helpers.js',
            './scripts/search/search-filters.js',
            './scripts/search/search-pagination.js',
            './scripts/search/search-result-info.js',
            './scripts/search/search-result-content.js',
            './scripts/search/search-init.js'
        ])
            .pipe(concat('search-index.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(path.productionFolder + '/assets/scripts/'));

        var searchVacatures = gulp.src([
            './scripts/search-vacatures/search-url-parameters.js',
            './scripts/search-vacatures/search-checkboxes.js',
            './scripts/search-vacatures/search-dropdown.js',
            './scripts/search-vacatures/search-helpers.js',
            './scripts/search-vacatures/search-filters.js',
            './scripts/search-vacatures/search-pagination.js',
            './scripts/search-vacatures/search-result-info.js',
            './scripts/search-vacatures/search-result-content.js',
            './scripts/search-vacatures/search-init.js',
            path.projectFolder + '/scripts/menu/menu-wb.js'
        ])
            .pipe(concat('vacatures-werkenbij.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var graph = gulp.src([
            './scripts/graphs/_polyfill.js',
            './scripts/graphs/_sluggify.js',
            './scripts/graphs/_colours.js',
            './scripts/graphs/_formats.js',
            './scripts/graphs/_helpers.js',
            './scripts/graphs/chart-init-objects.js',
            './scripts/graphs/chart-dimensions.js',
            './scripts/graphs/chart-svg.js',
            './scripts/graphs/chart-x-scale.js',
            './scripts/graphs/chart-y-scale.js',
            './scripts/graphs/chart-axis.js',
            './scripts/graphs/chart-legend.js',
            './scripts/graphs/chart-line.js',
            './scripts/graphs/chart-bar.js',
            './scripts/graphs/chart-bars-increase.js',
            './scripts/graphs/chart-stacked-bars.js',
            './scripts/graphs/chart-timeline-items.js',
            './scripts/graphs/instances/vacature-timeline.js'
        ])
            .pipe(concat('graphs.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(path.productionFolder + '/assets/scripts/'));


        return merge(page, theme, pageWB, homepage, homepageWB, post, page, vacature, ambassador, search, english, searchIndex, searchVacatures, graph);  // , map

    });
}
