'use strict';

const logger = require('../../services/logger.service');
const PagePersistence = require('../../persistence/page.persistence');
const moment = require('moment');

class MapDataService {


    constructor() {

        this.pagePersistence = new PagePersistence();
    }


    getLocationsAll() {

        let self = this;

        let relatedWorksOptions,
            options,
            mapData = {
                'all' : [],
                'filtered' : []
            };

        return new Promise(function(resolve, reject) {

                options = {
                    query : {
                        "type":"location",
                        "render_environments": { $in: ['amstelveenlijn'] }
                    },
                    "sort": {order: 1}
                };

                self.pagePersistence.find(options).then( function(locations) {
                    let mapDataAll = self._matchWorks(locations);
                    resolve(mapDataAll);
                });
        });
    }

    getLocationsFiltered() {

        let self = this;

        let relatedWorksOptions,
            options,
            mapData = {
                'all' : [],
                'filtered' : []
            };

        return new Promise(function(resolve, reject) {

            options = {
                query : {
                    "type":"location",
                    "render_environments": { $in: ['amstelveenlijn'] }
                },
                "sort": {order: 1}
            };

            self.pagePersistence.find(options).then( function(locations) {

                let mapDataFiltered = self._matchTopicalWorks(locations);
                resolve(mapDataFiltered);
            });
        });
    }

    _matchWorks(locations) {

        let self = this,
            options;

        return Promise.all(locations.map(function (location) {

            return new Promise(function (resolve, reject) {

                options = {
                    query : {
                        "type":"work",
                        "taxonomies.locations.slug" : location.slug
                    },
                    "sort": { "start": 1}
                };

                self.pagePersistence.find(options).then( function(works) {

                    // logger.info(works);

                    location.works = works;
                    resolve(location);
                });
            });
        }));
    }


    _matchTopicalWorks(locations) {

        let self = this,
            options;

        let now = moment();

        let rubicon = new Date(now.add(3, 'months'));
        let today = new Date(now);

        // samenvattend . moment is leuk voor optellen. Maar het moet een js Date object zijn dat in de query naar ISOString wordt gecast

        return Promise.all(locations.map(function (location) {

            return new Promise(function (resolve, reject) {

                options = {
                    query : {
                        "type":"work",
                        "taxonomies.locations.slug" : location.slug,
                        "start" : { "$lte": rubicon.toISOString() }
                        // ,
                        // "end" : { "$gte": today.toISOString() }
                    },
                    "sort": { "start": 1}
                };

                self.pagePersistence.find(options).then( function(works) {
                    location.works = works;
                    resolve(location);
                });
            });
        }));
    }
}

module.exports = MapDataService;