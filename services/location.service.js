'use strict';

const PagePersistence = require('../../persistence/page.persistence');
const fs = require('fs');
const appConfig = require('../../config/');
const logger = require('../../services/logger.service');

/**
 * Service for getting locations for the map navigation
 */
class LocationsService {

    constructor() {}

    async createGeojson(data) {

        let locations = await this._getLocations();
        let features = this._createFeatures(locations, 'location');
        let geojson = {
            "type": "FeatureCollection",
            "features": features
        };

        await this._writeFile(geojson, 'locations');

        return;
    }

    _createFeatures (objects, type) {
        let features = [];
        objects.forEach( (object) => {

            let properties = {
                "name": object.title,
                "slug": object.slug,
                "town": object.town,
                "address": object.location.address,
                "types": object.types,
                "stopType": object.stopType,
                "crossing": false,
                "url": object.url,
                "type" : type,
                "renderEnv": object.render_environments[0]
            };

            if(object.taxonomies['location-types'].indexOf('kruispunt') > -1) {
                properties.crossing = true;
            }

            let point = this._createPoint(properties, object.location.lng, object.location.lat)

            features.push(point);
        });
        return features
    }

    async _getLocations(){

        const pagePersistence = new PagePersistence();

        let options = {
            query: {
                type: "location"
            },
            "sort": {order: -1}
        };

        return await pagePersistence.find(options);
    }

    _createPoint(properties, lat, lng){
        lat = parseFloat(lat);
        lng = parseFloat(lng);
        return {
            "type": "Feature",
            "properties": properties,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    lat,
                    lng
                ]
            }
        }
    }

    async _writeFile(data, filename) {

        let url = appConfig.dist + 'amsteltram/assets/geojson/' + filename + '.geojson';

        await fs.writeFile(url, JSON.stringify(data), (err) => {
            // if (err) throw err;
            logger.info(filename + '.geojson has been created');
        });

        return;
    }
}

module.exports = LocationsService;
