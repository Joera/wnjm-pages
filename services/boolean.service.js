'use strict';


/**
 * Service for authorization of the incoming api calls
 */
class BooleanService {

    correct(value) {
        const self = this;

        if (value === true || value === false) {
            return value;
        } else if (value === '1' || value > 0 ) {
            return true;
        } else if (value === '0' || value < 1 ) {
            return false;
        } else {
            return false;
        }
    }
}

module.exports = BooleanService;