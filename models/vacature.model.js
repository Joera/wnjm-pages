
const logger = require('../../services/logger.service');
const booleanService = require('../services/boolean.service');
const _ = require('lodash');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'vacature',
            template: 'vacature.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return {
            _id: data.id,
            objectID: data.id, // id for algolia search
            type: data.type,
            slug: data.slug,
            url: data.link,
            status: data.status,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            main_image: data.main_image,
            date: data.date,
            sort_date: data.sort_date || data.date,
            modified: data.modified,
            author: data.author,
            sections: data.sections,
            taxonomies: data.taxonomies,
            render_environments: data.render_environments,
            vacature_parameters: data.vacature_parameters,
            vacature_teksten: data.vacature_teksten,
            vacature_tijdlijn: data.vacature_tijdlijn,
            vacature_contact: data.vacature_contact
        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data.id,
            template: 'vacature-snippet',
            type: 'vacature',
            slug: data.slug,
            url: data.link,
            title: data.title.rendered || data.title,
            date: data.date,
            sort_date: data.sort_date,
            content: data.content.rendered || data.content,
            render_environments: data.render_environments,
            vacature_parameters: data.vacature_parameters,
            vacature_teksten: null,
            vacature_tijdlijn: null,
            vacature_contact: null
        }


        return searchObject
    }

}




