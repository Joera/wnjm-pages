
const logger = require('../../services/logger.service');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'homepage',
            template: 'homepage.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {
        return {
            _id: data.id,
            objectID: data.id, // id for algolia search
            type: data.type,
            slug: data.slug,
            url: data.url, // replace wordpress generated url for page
            status: data.status,
            title: data.title,
            content: data.content.rendered || data.content,
            excerpt: data.excerpt.rendered || data.excerpt,
            sections: data.sections,
            date: data.date,
            modified: data.modified,
            main_image: data.main_image,
            render_environments: data.render_environments,
            featured_item: data.featured_item,
            featured_theme: data.featured_theme
        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data.id,
            template: 'search-snippet',
            type: 'page',
            slug: data.slug,
            url: data.link,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            render_environments: data.render_environments,
            language: data.language
        }

      //  typeof searchObject.sections === 'object' ? searchObject.sections = _.pickBy(searchObject.sections, (v) => { return v.type == 'paragraph'; }) : searchObject.sections = [];


        return searchObject;
    }
}
