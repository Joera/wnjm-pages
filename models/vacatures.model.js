const logger = require('../../services/logger.service');

module.exports = {


    getDefinition: () => {

        const definition = {
            name: 'vacatures',
            template: 'vacatures.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return {
                _id: data.id,
                objectID: data.id,
                type: data.type,
                slug: data.slug,
                url: data.link,
                status: data.status,
                main_image: data.main_image,
                title: data.title.rendered || data.title,
                content: data.content.rendered || data.content,
                render_environments: data.render_environments

            };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data.id,
            template: 'search-snippet',
            type: 'page',
            slug: data.slug,
            url: data.link,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            render_environments: data.render_environments

        };

        return searchObject;
    }
}
