
const logger = require('../../services/logger.service');
const _ = require('lodash');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'ambassador',
            template: 'ambassador.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return {

            _id: data.id,
            objectID: data.id, // id for algolia search
            type: data.type,
            slug: data.slug,
            url: data.link,
            status: data.status,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content.action,
            sections: data.sections,
            main_image: data.main_image,
            date: data.date,
            sort_date: data.sort_date || data.date,
            modified: data.modified,
            render_environments: data.render_environments,
            quote: data.quote,
            ambassador_fields:  data.ambassador_fields
        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data.id,
            template: 'search-snippet',
            type: 'ambassador',
            slug: data.slug,
            url: data.link,
            status: data.status,
            title: data.title.rendered || data.title,
            date: data.date,
            sort_date: data.sort_date,
            content: data.content.rendered || data.content,
            render_environments: data.render_environments
        }

    //      Array.isArray(searchObject.sections) ? searchObject.sections = searchObject.sections.filter( v => { return v.type == 'paragraph' }) : searchObject.sections = [];

        return searchObject
    }

}
