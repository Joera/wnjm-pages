
const logger = require('../../services/logger.service');
const slugify = require('slugify');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'document',
            template: ''
        }

        return definition;
    },

    getObjectMapping:  (document) => {

        const dataObject = {

            objectID : document.id, // keep algolia id consistent
            _id : document.id,
            type : 'document',
            title: document.title.rendered,
            // content: document.content.rendered,
            size: document.file.size,
            date: document.file.date,
            url: document.cdn.url,
            render_environments: []
            // tags: document['file_tags'],
            // posts : document['posts'],
            // document_history : document['document_history']
        }

        return dataObject
    },

    getSearchMapping: (document) => {

        const searchObject = {

            objectID : document.id, // keep algolia id consistent
            _id : document.id,
            type : 'document',
            template: 'document-snippet',
            title: document.title,
            slug: slugify(document.title),
            // content: document.content.rendered,
            size: document.size,
            date: document.date.date,
            url: document.url,
            render_environments: document.render_environments
            // tags: document['file_tags'],
            // posts : document['posts'],
            // document_history : document['document_history']
        }

        return searchObject
    }
}
