const PagePersistence = require('../../persistence/page.persistence');
const PathService = require('../../services/path.service');
const logger = require('../../services/logger.service');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'archive',
            template: 'archive.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return {
            _id: data.id,
            objectID: data.id, // id for algolia search
            type: data.type,
            slug: data.slug,
            url: data.url, // replace wordpress generated url for page
            status: data.status,
            title: data.title,
            content: data.content,
            excerpt: data.excerpt,
            date: data.date,
            modified: data.modified,
            categories: data.categories,
            static: data.static,
            catIds: (data.categories) ? data.categories.map( c => { return c.id }) : '',
            catSlugs: (data.categories) ? data.categories.map( c => { return c.slug }) : '',
            tags: data.tags,
            author: data.author,
            comments: data.comments,
            comment_count: parseInt(data.commentcount) || 0,
            comment_status: data.comment_status,
            last_comment_date : data.last_comment_date,
            sections: data.sections,
            main_image: data.main_image,
            square_image: data.square_image,
            location: data.location,
            information_block: data.information_block,
        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            template: 'search-snippet',

        }

        return searchObject
    }

}
