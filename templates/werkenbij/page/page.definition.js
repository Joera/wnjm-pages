const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');

const logger = require('../../../../services/logger.service');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId, options) => {

        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();

            let organisationContentOptions = {
                "query" : {
                    "_id":"51868"
                }
            };

            let startersContentOptions = {
                "query" : {
                    "_id":"52027"
                }
            };

            let conditionsContentOptions = {
                "query" : {
                    "_id":"52028"
                }
            };

            let vacanciesContentOptions = {
                "query" : {
                    "_id":"52208"
                }
            };

            let organisationContent = pagePersistence.findOne(organisationContentOptions);
            let startersContent = pagePersistence.findOne(startersContentOptions);
            let conditionsContent = pagePersistence.findOne(conditionsContentOptions);
            let vacanciesContent = pagePersistence.findOne(vacanciesContentOptions);


            // const footerService = new FooterService();
            //
            // //    let pageContent = pagePersistence.find(options);
            // let footerContent = footerService.getContent(data.render_environments[0],'nl');
            //
            Promise.all([organisationContent,startersContent,conditionsContent,vacanciesContent]).then(values => {

                data.related = [];

                for (let value of values) {

                    if (value._id !== data._id) {
                        data.related.push(value);
                    }
                }
            });

            resolve(data);
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            const pathService = new PathService();
            const path = '/' + render_environment + '/' + pathService.cleanString(data.slug);
            // logger.info('Generated post path: ' + path, correlationId);
            resolve(path);
        })
    },

    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {
            logger.info('Get template dependencies for ' + data.type, correlationId);

            resolve([
                { template: 'homepage', data: null },
                { template: 'search', data: null }
            ]);
        })
    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}
