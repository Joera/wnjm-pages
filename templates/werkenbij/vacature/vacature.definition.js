const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const ShuffleService = require('../../../services/shuffle.service');
const logger = require('../../../../services/logger.service');
const moment = require('moment');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId) => {

        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            const shuffleService = new ShuffleService();

            let vacatureQuery = {
                query: {
                    type: "vacature",
                    slug: {$ne: data.slug},
                },
                "sort": {date: -1}
            }

            let ambassadorQuery = {
                query: {
                    type: "ambassador"
                },
                "sort": {date: -1}
            }

            let vacaturePosts = pagePersistence.find(vacatureQuery);
            let ambassadorPosts = pagePersistence.find(ambassadorQuery);

            Promise.all([vacaturePosts,ambassadorPosts]).then(values => {

                data.related_vacancies = shuffleService.shuffle(values[0]);
                data.ambassadors = shuffleService.shuffle(values[1]);
                resolve(data);
            });

            resolve(data);
        });
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page

			const year = moment(data.date, 'YYYY').format('YYYY');
            const pathService = new PathService();
            const path = '/' + render_environment + '/vacature/' + year + '/' + pathService.cleanString(data.slug);
            return path;
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            //logger.info('Get template dependencies for ' + data.type, correlationId);
            const pagePersistence = new PagePersistence();

            let depArray = [];

            let vacatureQuery = {
                query: {
                    type: "vacature",
                    status: "publish"
                },
                "sort": {date: -1},
                "limit": 6
            };

            let ambassadorQuery = {
                query: {
                    type: "ambassador",
                    status: "publish"
                },
                "sort": {date: -1}
            };

            let vacaturePosts = pagePersistence.find(vacatureQuery);

            let ambassadorPosts = pagePersistence.find(ambassadorQuery);

            Promise.all([vacaturePosts,ambassadorPosts]).then(values => {
                for (let val of values[0]) {
                    depArray.push({ template: 'vacature', data: val })
                }
                for (let val of values[1]) {
                    depArray.push({ template: 'ambassador', data: val })
                }
                depArray.push({ template: 'homepage', data: null });
                depArray.push({ template: 'vacatures', data: null });
                resolve(depArray);
            });
        })
    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}
