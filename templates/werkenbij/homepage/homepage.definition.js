const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            // const footerService = new FooterService();

            let homeContentOptions = {
                "query" : {
                    "_id":"51427"
                }
            };

            let organisationContentOptions = {
                "query" : {
                    "_id":"51868"
                }
            };

            let startersContentOptions = {
                "query" : {
                    "_id":"52027"
                }
            };

            let conditionsContentOptions = {
                "query" : {
                    "_id":"52028"
                }
            };



            let homeContent = pagePersistence.findOne(homeContentOptions);
            let organisationContent = pagePersistence.findOne(organisationContentOptions);
            let startersContent = pagePersistence.findOne(startersContentOptions);
            let conditionsContent = pagePersistence.findOne(conditionsContentOptions);



            let ambassadorQuery = {
                query: {
                    type: "ambassador",
                    status: "publish"
                },
                "sort": {date: -1},
                "limit": 3
            }
            //
            let vacancyOptions = {
                "query" : {
                    "type": { $in: ["vacature"] }
                },
                "sort": {'date': -1},
                "limit": 3
            };

            let findVacancies = pagePersistence.find(vacancyOptions);
            let findAmbassadors= pagePersistence.find(ambassadorQuery);

			Promise.all([homeContent,organisationContent,startersContent,conditionsContent,findVacancies,findAmbassadors]).then(values => {

			    data.homeContent = values[0];
                data.organisation = values[1];
                data.starters = values[2];
                data.conditions = values[3];
			    data.vacancies = values[4];
                data.ambassadors = values[5];
				resolve(data)
			});
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            logger.info('Generated post path: ' + render_environment, correlationId);
            resolve('/' + render_environment);
        })
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {
            logger.info('Get template dependencies for ' + data.type, correlationId);
            // [{template: '', param: ''}]
            // resolve([{template: 'home', data: null}, {template: 'news', data: null}]);
            resolve([]);
        })

    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}
