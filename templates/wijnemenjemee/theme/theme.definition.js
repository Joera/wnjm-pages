const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const ConversationService = require('../../../services/conversation.service');

const logger = require('../../../../services/logger.service');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId, options) => {

        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            const footerService = new FooterService();
            const conversationService = new ConversationService();

            let BlogOptions = {
                "query" : {
                    "type": { $in: ["post"] },
                    "tagIds": { $in: [data.theme_fields.tagID]}

                },
                "sort": {'sort_date': -1},
                "limit": 11
            };

            let findPosts = pagePersistence.find(BlogOptions);
            let footerContent = footerService.getContent(data.render_environments[0],'nl');

            Promise.all([findPosts,footerContent]).then(values => {

                data.posts = values[0];
                data.footer = values[1];

                data.comments = conversationService.collectComments(values[0]);
                data.comments.sort(function (a, b) {

                    if (a.date < b.date) {
                        return 1;
                    }
                    if (a.date > b.date) {
                        return -1;
                    }
                });

                resolve(data);
            });
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            const pathService = new PathService();
            const path = '/' + pathService.cleanString(data.slug);  // render_environment + '/' +
            // logger.info('Generated post path: ' + path, correlationId);
            resolve(path);
        })
    },

    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {

            resolve([{template: 'homepage', data: null}]);
        })
    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}
