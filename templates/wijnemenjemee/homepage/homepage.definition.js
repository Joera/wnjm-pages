const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');
const ConversationService = require('../../../services/conversation.service');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            const conversationService = new ConversationService();
            const footerService = new FooterService();

            let homeContentOptions = {
                "query" : {
                    "_id":"14209"
                }
            };
            let homeContent = pagePersistence.findOne(homeContentOptions);

            let BlogOptions = {
                "query" : {
                    "type": { $in: ["post"] },
                    "featured_item": { $nin : [true,1,'1']},
                    "status": "publish"
                },
                "sort": {'sort_date': -1},
                "limit": 16
            };

            let stickyOptions = {
                "query" : {
                    "type": { $in: ["post"] },
                    "featured_item": { $in : [true,1,'1']},
                    "status": "publish"
                },
                "sort": {'date': -1}
            };


            let findPosts = pagePersistence.find(BlogOptions);
            let findStickyPost = pagePersistence.find(stickyOptions);

            let commentPostOptions = {
                query : {
                    "type": { $in: ["post","page"] }
                }
            };

            let findCommentPosts = pagePersistence.find(commentPostOptions);
         //   let footerContent = footerService.getContent(data.render_environments[0],'nl');


			Promise.all([homeContent,findPosts,findStickyPost,findCommentPosts]).then(values => {

			    data = values[0];
				data.posts = values[1];
				data.sticky = values[2];
				data.comments = conversationService.collectComments(values[3]);
             //   data.footer = values[4];

                if (data.sticky && data.sticky.length > 0) {
                    data.posts = data.sticky.concat(data.posts);
                }

                data.comments.sort(function (a, b) {

                    if (a.date < b.date) {
                        return 1;
                    }
                    if (a.date > b.date) {
                        return -1;
                    }
                });

                pagePersistence.findOne({query: {_id: data.featured_theme}})
                .then( (theme) => {
                    data.theme = theme;
                    resolve(data)
                });
			})
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            // logger.info('Generated post path: ' + path, correlationId);
       //     resolve(render_environment);
            resolve('/');
        })
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {
            logger.info('Get template dependencies for ' + data.type, correlationId);
            // [{template: '', param: ''}]
            // resolve([{template: 'home', data: null}, {template: 'news', data: null}]);
            resolve([]);
        })

    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}
